module dynamics
    
contains

  ! ###################    
  subroutine integrate1(rn, links, links3, disl, nb_neigbs, ele_neigbs, Psolutes, &
       appliedstress, vn, dt, convergent, grid_pos, Ps_grid, nPS_grid)
    
    use param
    use utils
    implicit none
    
    real(kind=8)    ::  rn(Nntot,3), rnew(Nntot,3), vn(Nntot,3), appliedstress(3,3), &
         dt, Psolutes(Nstot,3)
    integer         ::  links(Nntot-1,2), links3(Nntot,3), disl(Ndtot,2),nb_neigbs(Nntot), &
         ele_neigbs(Nele_ngbs,Nele_ngbs), grid_pos(nntot,2), Ps_grid(nngridtot,nngridtot,Nsgridloc), nPS_grid(nngridtot,nngridtot)
    logical         ::  convergent
        
    intent(in)      ::  links, links3, disl, appliedstress, Psolutes
    intent(out)     ::  vn, dt, convergent
    intent(inout)   ::  rn
    
    dt = dt0
    !call drdt(rn, links, links3, disl, distMat, distMatID, appliedstress, Psolutes, vn)
    call drdt1(rn, links, links3, disl, nb_neigbs, ele_neigbs, appliedstress, Psolutes, vn, grid_pos, Ps_grid, nPS_grid)
    
    ! Euler-forward integration scheme
    rnew = rn + vn*dt0
    convergent= .TRUE.
    rn = rnew
    
  end subroutine integrate1
  ! ###################   
  
  ! ###################   
  ! ###################
    

  ! ###################   
  subroutine drdt1(rn, links, links3, disl, nb_neigbs, ele_neigbs, appliedstress, Psolutes, vn, grid_pos, Ps_grid, nPS_grid)
    
    use param
    use utils
    implicit none
    
    real(kind=8)    ::  rn(Nntot,3), vn(Nntot,3), appliedstress(3,3), &
         fn(Nntot,3), dt, Psolutes(Nstot,3)
    integer         ::  links(Nntot-1,2), links3(Nntot,3), disl(Ndtot,2), &
         nb_neigbs(Nntot), ele_neigbs(Nele_ngbs,Nele_ngbs), grid_pos(nntot,2), &
         Ps_grid(nngridtot,nngridtot,Nsgridloc), nPS_grid(nngridtot,nngridtot)
    
    intent(in)      ::  links, links3, disl, appliedstress, Psolutes, rn
    intent(out)     ::  vn
    
    select case (method)
    case ('CAI')
       call segforcevec(rn, links, disl, appliedstress, fn)
    case ('BJO')
          call curvatureforce1(rn, links3, disl, nb_neigbs, ele_neigbs, appliedstress, Psolutes, fn, grid_pos, Ps_grid, nPS_grid)
    end select
    
    call mobility(fn, vn)
    
  end subroutine drdt1
  ! ###################
  ! ###################   
  ! ###################
  
  
  ! ###################    	
  subroutine mobility(fn, vn)
    
    use param
    implicit none
    
    real(kind=8)	 ::  vn(Nntot,3), fn(Nntot,3)
    integer 		 ::  i
    
    intent(in)		 ::  fn
    intent(out)		 ::  vn  
    
    ! apply drag matrix B on force fn and compute velocity vn
    ! define the mobility rules here
    vn = fn
    
    ! fixed nodes get zero velocity
    do i=1, size(fixed)
       if (fixed(i) .GE. 1) then
          vn(fixed(i),:) = 0.d0
       else
          exit
       end if
    end do
    
    
  end subroutine mobility
  ! ###################
  
  
  ! ###################

  subroutine curvatureforce1(rn, links3, disl, nb_neigbs, ele_neigbs, appliedstress, Psolutes, fn, grid_pos, Ps_grid, nPS_grid)
    
    use param
    use utils
    implicit none
	
    real(kind=8)	::  s(3,3), stot(3,3), rn(Nntot,3), fn(Nntot,3), t(3), &
         sigb(3), nvec(3), tvec(3), Gm, invR, sols(3,3), &
         appliedstress(3,3), fproj, Psolutes(Nstot,3), &
         rn1(3), rn2(3), rn3(3), ftmp(3), &
         nvec_slip(3), burger(3)
    integer 		::  links3(Nntot,3), i, localnode, node, &
         disl(Ndtot,2),  nb_neigbs(Nntot), ele_neigbs(Nele_ngbs,Nele_ngbs),&
         locnodes(5), dID, irn1, irn2, irn3,&
         grid_pos(nntot,2), Ps_grid(nngridtot,nngridtot,Nsgridloc), nPS_grid(nngridtot,nngridtot)
    
    intent(in)		::  rn, links3, disl, appliedstress, Psolutes
    intent(out)		::  fn
    
    fn = 0.d0
        
    ! main outerloop over all the nodes
    do i = 1, Nnodes
       irn1 = links3(i,1)
       irn2 = links3(i,2)
       irn3 = links3(i,3)
       rn1 = rn(irn1,:)
       rn2 = rn(irn2,:)
       rn3 = rn(irn3,:)
       node = irn2
       
       if (Bcond .EQ. 'PER') then          ! if we deal with the node on the other side of periodic box, if so, reflect it
          call reflect3p(rn1, rn2, rn3)
       end if
       if (isfixed(node)) then  ! if node is fixed, nodal force is zero (not correct, need to be fixed)
          fn(node,:) = 0.d0
          cycle
       end if
       ! caluclate curvature "invR", slip vector "nvec" and tangent vector "tvec"
       call circle3P(rn1, rn2, rn3, invR, nvec, tvec)
       ! get stress at point rn2 from all dislocation segments


       

! TURN OFF OR ON DISLOCATION SELF STRESS
!      call segstressAtP1(rn, links3, disl, rn2, s, irn2, nb_neigbs, ele_neigbs)
       s = 0.d0


       
       ! get stress at point rn2 from all solutes 
       !call solutestress(Psolutes, rn2, sols)
       call solutestress1(Psolutes, rn2, sols, irn2, grid_pos, Ps_grid, nPS_grid)
       !sols = 0.d0
       stot = s + appliedstress + sols

       ! treat burgers vectors for PARTIALS
       if (analysis .EQ. 'PARTIALS') then
          dID = getdislocID(node, disl)
          if (dID .EQ. 1) then
             burger = bvlead
          else if (dID .EQ. 2) then
             burger = bvtrail
          else
             write(*,*) 'Wrong dislocID identified. Exiting...'
             stop
          end if
       else
          burger = bv
       end if
       
       ! superpose segment stress s with the externally appliedstress 
       sigb = mdot(stot, burger)
       ! self energy of infinitely dislocation line
       Gm = (MU*vdot(burger,burger)/(2.d0*(1.d0-NU)))*(1.d0-NU*(vdot(tvec,burger))**2/vdot(burger,burger))
       Gm = Gm*(1.d0+corefactor)
       ! projection of PK-force to nvec direction
       ftmp = cross(sigb, tvec)

       ! projection of the nvec, i.e. normal vector of the circle in circle's plane onto the slip plane given by normal sv
       nvec_slip = cross(cross(sv, nvec),sv)
       ! projction of the total force onto the slip plane
       fproj = vdot(ftmp, nvec_slip)
       ! line tension is projected also on the slip plane, that is why it is mulitplied by vdot(nvec,nvec_slip)
       fn(node,:) = (fproj - invR*Gm*vdot(nvec,nvec_slip))*nvec_slip
       
       ! treating the PARTIALS
       if (analysis .EQ. 'PARTIALS') then
          if (dID .EQ. 1) then
             !fn(node,:) = fn(node,:) + sfe*offset
             fn(node,:) = fn(node,:) + sfe*cross(sv,tvec)
          else if (dID .EQ. 2) then
             !fn(node,:) = fn(node,:) - sfe*offset
             fn(node,:) = fn(node,:) - sfe*cross(sv,tvec)
          end if
       end if
    end do
    
  end subroutine curvatureforce1
  ! ###################
  ! ###################
  ! ###################
  ! ###################    
  subroutine mirror(x)

    use param
    use utils
    implicit none
    
    real(kind=8)	::  x(3), proj(3)
    
    intent(inout)      ::  x
    
    ! do mirroring a vector x around the plane given by normal pervec
    !proj = vdot(x, pervec) * pervec*boxsize
    !x = x - 2.d0*proj
    proj = vdot(x, pervec) * pervec
    x = x - 2.d0*proj
    
  end subroutine mirror
  ! ###################       
        
  ! ###################    
  subroutine reflect(x)
    
    use param
    use utils
    implicit none
    
    real(kind=8)	::  x(3)
    
    intent(inout)   ::  x
    
    if ( vdot(x, pervec) .GT. 0.d0 ) then
       x = x - boxsize*pervec
    else
       x = x + boxsize*pervec
    end if
    
  end subroutine reflect
  ! ################### 


  ! ###################    
  subroutine reflect3p(pA,pB,pC)
    
    use param
    use utils
    implicit none
    
    real(kind=8)    ::  pA(3), pB(3), pC(3)
    
    intent(inout)   ::  pA, pB, pC
    
    if (norm(pA-pB) .GT. 0.5d0*boxsize) then
       call reflect(pA)
    else if (norm(pC-pB) .GT. 0.5d0*boxsize) then
       call reflect(pC)
    end if
    
  end subroutine reflect3p
  ! ################### 
  
  
  ! ###################    
  ! ################### 
    
  !           call segstressAtP1(rn, links3, disl, rn2, s, irn2, nb_neigbs, ele_neigbs)

  ! ###################
  ! Calculate stress from the entire dislocation network at point r
  subroutine segstressAtP1(rn, links3, disl, r, s, ir, nb_neigbs, ele_neigbs)
      
    use param
    use utils
    implicit none
    
    real(kind=8)    ::  rn(Nntot,3), r(3), s(3,3), stemp(3,3), &
         pB(3), pC(3), burger(3), proj_pB, proj_pC
    integer         ::  links3(Nntot,3), id, disl(Ndtot,2), i, ir, &
         ele, dID, nb_neigbs(Nntot), ele_neigbs(Nele_ngbs,Nele_ngbs)
    
    intent(in)      ::  rn, links3, disl, r
    intent(out)     ::  s
    
    s = 0.d0
    
    ! loop over all the elements
    do i = 1, nb_neigbs(ir)
         ele = ele_neigbs(ir,i)
         pB = rn(links3(ele,2),:)
         pC = rn(links3(ele,3),:)
         if ((Bcond .EQ. 'PER') .AND. (norm(pB-pC) .GT. 0.5d0*boxsize)) then     ! check if we deal with the node on the other side of periodic box, if so, reflect it
            call reflect(pC)
         end if
         if ( isfixed(links3(ele,2)) .AND. isfixed(links3(ele,3)) ) then    ! if element is between two fixed nodes, this gives no contribution to stress
            cycle
         end if
         ! skip elements: ele=Nnodes/2 and ele=Nnodes, e.g. from Nnodes/2 to Nnodes/2 +1
         !if (sqrt(vdot(pB-pC,pB-pC)).ge.0.5d0*boxsize) cycle
         if (analysis .EQ. 'PARTIALS') then
            dID = getdislocID(ele, disl)
            if (dID .EQ. 1) then
               burger = bvlead
            else if (dID .EQ. 2) then
               burger = bvtrail
            else
               write(*,*) 'Wrong dislocID identified. Exiting...'
               stop
            end if
         else
            burger = bv
            ! for NORMAL analysis
         end if
         call calsegstr3da(pB, pC, r, burger, stemp)
         s = s + stemp
         
         If(Bcond .eq. 'PER') then
            ! if the element is within the REFLECT_RATIO*BOXSIZE distance from the periodic boundary
            ! then reflect it and add its contribution to the stress
            proj_pB = vdot(pB,pervec)
            proj_pC = vdot(pC,pervec)
            if ((0.5d0-abs(0.5d0*(proj_pB+proj_pc))/boxsize) .LT. reflect_ratio) then
               
               ! pB =(proj_pB - sign(boxsize,proj_pB))*pervec
               ! pC =  (proj_pC - sign(boxsize,proj_pC))*pervec
               pB = pB - sign(boxsize,proj_pB)*pervec
               pC = pC  - sign(boxsize,proj_pC)*pervec
               call calsegstr3da(pB, pC, r, burger, stemp)
               s = s + stemp
            end if
         endif         
      end do
    end subroutine segstressAtP1
    ! ###################
    ! ###################
    ! ###################
    ! ###################    
        subroutine calsegstr3da(r1, r2, r, b, s)
        
        use utils
	    implicit none
        
        real(kind=8)    ::  r1(3), r2(3), r(3), s(3,3), b(3), &
                            r21(3), rt(3), e1(3), e2(3), e3(3), M(3,3), Mt(3,3), &
                            rr21(3), rr(3), br(3), norm21, sr(3,3)
        
        intent(in)      ::  r1, r2, r
        intent(out)     ::  s
        
        r21 = r2 - r1
        rt  = r  - r1

        norm21 = norm(r21)
        if (norm21 .LT. 1.d-20) then
          s = 0.d0
        end if

        e1 = r21/norm21
        e2 = schmidt(rt, e1)
        e3 = cross(e1, e2)
        e3 = e3/norm(e3)

        M(:,1) = e2
        M(:,2) = e3
        M(:,3) = e1

        Mt = mtransp(M)

        rr21 = mdot(Mt, r21)
        rr   = mdot(Mt, rt)
        br   = mdot(Mt, b)	

        call calsegstrhor3da(br(1), br(2), br(3), 0.d0, rr21(3), rr(1), rr(3), sr)
        s    = transform(sr, M)


        end subroutine calsegstr3da
        ! ###################
        


        ! ###################         
        subroutine calsegstrhor3da(bx, by, bz, z1, z2, x, z, ss)
		! based on Hirth-Lothe Eq.(5-45) p.134 (y.EQ.0)
		! with self consistent smoothing R->Ra
		
        use param, only: a, MU, NU
        use utils
	    implicit none
        
        real(kind=8)    ::  bx, by, bz, z1, z2, x, z, ss(3,3), &
                            l, lp, dx, dx2, a2, ra, ra2, ra3, rap, rap2, rap3, &
                            sunit, s(6), invral, invrapl, rhoa2
        integer         ::  form

        intent(in)      ::  bx, by, bz, z1, z2, x, z
        intent(out)     ::  ss

        
        l  = z1 - z
        lp = z2 - z
        dx = x
        dx2 = dx**2
        a2  = a**2
        
        ra  = sqrt(dx2 + l**2  + a2)
        rap = sqrt(dx2 + lp**2 + a2)

        rap2 = rap**2
        rap3 = rap**3
        ra2  = ra**2
        ra3  = ra**3

        sunit = MU*(1.d0/4.d0/pivalue/(1.d0-NU))

        s = 0.d0
        
        if ((l .GT. 0.d0) .and. (lp .GT. 0)) then
            form = 1
        elseif ((l .LT. 0.d0) .and. (lp .LT. 0.d0)) then
            form = 2
        else
            form = 3
        end if
        
        select case (form)
        case(1)
			!write(*,*) 'case 1'
            invral  = 1.d0/ra/(ra+l)
            invrapl = 1.d0/rap/(rap+lp)
            s(1)    = by*dx*( invrapl*(1.d0-(dx2+a2)/rap2-(dx2+a2)*invrapl) - &
                      invral *(1.d0-(dx2+a2)/ra2 -(dx2+a2)*invral) )
            s(2)    = -bx*dx*( invrapl - invral )
            s(3)    = by*( (-NU/rap+dx2/rap3+(1.d0-NU)*(a2/2.d0)/rap3) - &
                      (-NU/ra +dx2/ra3 +(1.d0-NU)*(a2/2.d0)/ra3 ) )
            s(4)    = -by*dx*( invrapl*(1.d0+a2/rap2+a2*invrapl) - &
                      invral *(1.d0+a2/ra2 +a2*invral ) )
            s(5)    = ( bx*(NU/rap-(1.d0-NU)*(a2/2.d0)/rap3) - bz*dx*(1.d0-NU)*invrapl*(1.d0+(a2/2.d0)/rap2+(a2/2.d0)*invrapl) ) - &
                      ( bx*(NU/ra -(1.d0-NU)*(a2/2.d0)/ra3 ) - bz*dx*(1.d0-NU)*invral *(1.d0+(a2/2.d0)/ra2 +(a2/2.d0)*invral ) )
            s(6)    = by*dx*( (-2.d0*NU*invrapl*(1.d0+(a2/2.d0)/rap2+(a2/2.d0)*invrapl)-lp/rap3) - &
                      (-2.d0*NU*invral *(1.d0+(a2/2.d0)/ra2 +(a2/2.d0)*invral )-l /ra3 ) )
        case(2)
			!write(*,*) 'case 2'
            invral  = 1.d0/ra/(ra-l)
            invrapl = 1.d0/rap/(rap-lp)
            s(1)    = -by*dx*( invrapl*(1.d0-(dx2+a2)/rap2-(dx2+a2)*invrapl) -&
                      invral *(1.d0-(dx2+a2)/ra2 -(dx2+a2)*invral) )
            s(2)    = +bx*dx*( invrapl - invral )
            s(3)    = by*( (-NU/rap+dx2/rap3+(1.d0-NU)*(a2/2.d0)/rap3) - &
                      (-NU/ra +dx2/ra3 +(1.d0-NU)*(a2/2)/ra3 ) )
            s(4)    = by*dx*( invrapl*(1.d0+a2/rap2+a2*invrapl) - &
                      invral *(1.d0+a2/ra2 +a2*invral ) )
            s(5)    = ( bx*(NU/rap-(1.d0-NU)*(a2/2.d0)/rap3) + bz*dx*(1.d0-NU)*invrapl*(1.d0+(a2/2.d0)/rap2+(a2/2.d0)*invrapl) ) - &
                      ( bx*(NU/ra -(1.d0-NU)*(a2/2.d0)/ra3 ) + bz*dx*(1.d0-NU)*invral *(1.d0+(a2/2.d0)/ra2 +(a2/2.d0)*invral ) )
            s(6)    = by*dx*( (2.d0*NU*invrapl*(1.d0+(a2/2.d0)/rap2+(a2/2.d0)*invrapl)-lp/rap3) - &
                      (2*NU*invral *(1.d0+(a2/2.d0)/ra2 +(a2/2.d0)*invral )-l /ra3 ) )
        case(3)
			!write(*,*) 'case 3'
            rhoa2   = dx2+a2
            s(1)    = by*dx/rhoa2 *( lp/rap*(-1.d0+2.d0*(dx2+a2)/rhoa2+(dx2+a2)/rap2) - &
                      l /ra *(-1.d0+2.d0*(dx2+a2)/rhoa2+(dx2+a2)/ra2 ) )
            s(2)    = +bx*dx/rhoa2*( lp/rap - l/ra )
            s(3)    = by*( (-NU/rap+dx2/rap3+(1.d0-NU)*(a2/2.d0)/rap3) - &
                      (-NU/ra +dx2/ra3 +(1.d0-NU)*(a2/2.d0)/ra3 ) )  
            s(4)    = by*dx/rhoa2*( lp/rap*(1.d0+(a2*2.d0)/rhoa2+(a2)/rap/rap) - &
                      l/ra *(1.d0+(a2*2.d0)/rhoa2+(a2)/ra/ra ) )
            s(5)    = ( bx*(NU/rap -(1.d0-NU)*(a2/2.d0)/rap3) + bz*dx/rhoa2*(1.d0-NU)*lp/rap*(1.d0+(a2)/rhoa2+(a2/2.d0)/rap2) ) - &
                      ( bx*(NU/ra  -(1.d0-NU)*(a2/2.d0)/ra3 ) + bz*dx/rhoa2*(1.d0-NU)*l /ra *(1.d0+(a2)/rhoa2+(a2/2.d0)/ra2 ) )
            s(6)    = by*dx*( (2.d0*NU*lp/rhoa2/rap*(1.d0+(a2)/rhoa2+(a2/2.d0)/rap2) - lp/rap3) - &
                      (2.d0*NU*l /rhoa2/ra *(1.d0+(a2)/rhoa2+(a2/2.d0)/ra2 ) - l /ra3 ) )
        end select

        ss(1,1) = s(1)
        ss(1,2) = s(2)
        ss(1,3) = s(3)
        ss(2,2) = s(4)
        ss(2,3) = s(5)
        ss(3,3) = s(6)
        ss(2,1) = ss(1,2)
        ss(3,1) = ss(1,3)
        ss(3,2) = ss(2,3)
        ss = ss*sunit

        end subroutine calsegstrhor3da
    ! ###################   

        
        
    ! ###################    
        subroutine circle3P(pA, pB, pC, invR, nvec, tvec)
        
        use param
        use utils
	    implicit none
        
        real(kind=8)    ::  pA(3), pB(3), pC(3), invR, &
                            xA, yA, xB, yB, xC, yC, AB(3), BC(3), CA(3), &
                            nvec(3), tvec(3), np(3), ang, &
                            tmpA, tmpB, tmpalpha, tmpbeta, tmpgamma, &
                            nAB, nBC, nCA
        
        intent(in)      ::  pA, pB, pC
        intent(out)     ::  invR, nvec, tvec

        AB = pB-pA
        BC = pC-pB
        CA = pA-pC
 
        nAB = norm(AB)
        nBC = norm(BC)
        nCA = norm(CA)
 
        ang = abs(vdot(AB,BC)/(nAB*nBC)-1.d0)
       ! if (ang .GT. 1.d-12) then
        if (ang .GT. 1.d-14) then
            tmpA = norm(cross(AB,BC))
            tmpB = nAB*nBC*nCA
            tmpalpha = nBC**2 *vdot(-AB,CA)
            tmpbeta  = nCA**2 *vdot(AB,-BC)
            tmpgamma = nAB**2 *vdot(-CA,BC)
            invR = 2*tmpA/tmpB
            nvec = 1.d0/(tmpA*tmpB) *(-tmpalpha*pA + (2.d0*tmpA**2-tmpbeta)*pB - tmpgamma*pC)
            tvec = 1.d0/tmpA * cross(cross(AB,BC),nvec)
            nvec = nvec/norm(nvec)
            tvec = tvec/norm(tvec)
        else 
            invR = 0.d0
            nvec = cross(CA/nCA, sv)
            tvec = -CA/nCA
        end if
        
        end subroutine circle3P
    ! ###################    
    

    ! ###################    
        subroutine gennode(pA, pB, pC, position, newnode, reflected)
        
        use param
        use utils
        implicit none
        
        real(kind=8)    ::  pA(3), pB(3), pC(3), invR, newnode(3), tmp(3), nvec(3), tvec(3), &
                            cent(3), R
        integer         ::  position
        logical         ::  reflected

        intent(in)      ::  pA, pB, pC, position
        intent(out)     ::  newnode, reflected

        reflected = .FALSE.
        call circle3P(pA, pB, pC, invR, nvec, tvec)
        R = 1.d0/invR
        cent = pB - R*nvec
        if (position .EQ. 1) then
            tmp = (pB + pA)/2.d0 - cent
        else if (position .EQ. 2) then
            tmp = (pB + pC)/2.d0 - cent
        end if
        newnode = cent + R*tmp/norm(tmp)   

        ! if newnode happened to be outside of the periodic box, will be reflected inwards
        if ((Bcond .EQ. 'PER') .AND. (abs(vdot(newnode,pervec)) .GT. 0.5d0*boxsize)) then
            call reflect(newnode)
            reflected = .TRUE.
        end if
        
        end subroutine gennode
    ! ###################  


    ! ################### 
        function isfixed(node) result(logic)
        
        use param
        implicit none
        
        integer     :: node
        logical     :: logic
        
        intent(in)  :: node
        
        if (any(node .EQ. fixed)) then
            logic = .TRUE.
        else
            logic = .FALSE.
        end if
        
        end function isfixed
    ! ################### 


    ! ################### 
        function getdislocID(node, disl) result(dislocID)
        
        use param
        implicit none
        
        integer     :: node, disl(Ndtot,2), dislocID
        
        intent(in)  :: node, disl

        if (node .LE. disl(1,2)) then
            dislocID = 1
        else if (node .LE. disl(2,2)) then
            dislocID = 2
        else
            write(*,*) 'Node has ID not within the first two dislocations.'
            dislocID = -1
        end if
        
        end function getdislocID
    ! ################### 



    ! ###################           
        subroutine SelfForce(coreOnly, &
                             bx,  by,  bz, &
                             x1,  y1,  z1, &
                             x2,  y2,  z2, &
                             f1,  f2)
        ! this is adopted from Paradis 2.3.5.1 from NodeForces.c
        ! it should give the same as subroutine SelfForceMatlab  

        use param, only: a, MU, NU
        use utils
        implicit none
        
        real(kind=8) :: bx,  by,  bz, &
                        x1,  y1,  z1, &
                        x2,  y2,  z2, &
                        a0, Ec, &
                        f1(3),  f2(3), &
                        tx, ty, tz, L, La, S, &
                        bs, bs2, bex, bey, bez, be2, fL, ft
        logical      :: coreOnly

        intent(in)   :: coreOnly, bx,  by,  bz, x1,  y1,  z1, x2,  y2,  z2
        intent(out)  :: f1, f2
        
        bs  = bx*tx + by*ty + bz*tz
        bex = bx-bs*tx; bey = by-bs*ty; bez=bz-bs*tz
        be2 = (bex*bex+bey*bey+bez*bez)
        bs2 = bs*bs
    
        La = sqrt(L*L+a*a)
    
        if (coreOnly .EQV. .TRUE.) then
            S = 0.0d0
        else
            S = (-(2.d0*NU*La+(1.d0-NU)*a*a/La-(1.d0+NU)*a)/L + &
                (NU*log((La+L)/a)-(1.d0-NU)*0.5d0*L/La)) *MU/4.d0/pivalue/(1.d0-NU)*bs
        end if
        
        !a0 = a/500.d0
        a0 = 1.0
        Ec = MU/(4.d0*pivalue)*log(a/a0)
        ! Account for the self force due to the core energy change when 
        ! the core radius changes from a0-->a, M. Tang, 7/19/2004
        fL = -Ec*(bs2+be2/(1.d0-NU))
        ft =  Ec*2.d0*bs*NU/(1.d0-NU)  
        
        f2(1) = bex*(S+ft) + fL*tx
        f2(2) = bey*(S+ft) + fL*ty
        f2(3) = bez*(S+ft) + fL*tz
    
        f1(1) = -f2(1)
        f1(2) = -f2(2)
        f1(3) = -f2(3)
    
        end subroutine SelfForce
        
        
! *     /**************************************************************************
! *
! *      Function:    StressDueToSeg
! *      Description: Calculate the stress at point p from the segment
! *                   starting at point p1 and ending at point p2.
! *
! *      Arguments:
! *         px, py, pz     coordinates of field point at which stress is to
! *                        be evaluated
! *         p1x, p1y, p1z  starting position of the dislocation segment
! *         p2x, p2y, p2z  ending position of the dislocation segment
! *         bx, by, bz     burgers vector associated with segment going
! *                        from p1 to p2
! *         a              core value
! *         MU             shear modulus
! *         NU             poisson ratio
! *         stress         array of stresses form the indicated segment
! *                        at the field point requested
! *                            [0] = stressxx
! *                            [1] = stressyy
! *                            [2] = stresszz
! *                            [3] = stressxy
! *                            [4] = stressyz
! *                            [5] = stressxz
! *
! *************************************************************************/
        subroutine StressDueToSeg(px, py, pz, &
                                  p1x, p1y, p1z, &
                                  p2x, p2y, p2z, &
                                  bx, by, bz, &
                                  stress)
        ! this is taken from Paradis 2.3.5.1 from NodeForces.c
        ! it is coordinate independent version of subroutine calsegstr3da

        use utils
        use param, only: a, MU, NU
        implicit none

        real(kind=8) :: oneoverLp, common0, &
               px, py, pz, &
               p1x, p1y, p1z, &
               p2x, p2y, p2z, &
               bx, by, bz, &
               stress(3,3), &
               vec1x, vec1y, vec1z, &
               tpx, tpy, tpz, &
               Rx, Ry, Rz, Rdt, &
               ndx, ndy, ndz, &
               d2, s1, s2, a2, a2_d2, a2d2inv, &
               Ra, Rainv, Ra3inv, sRa3inv, &
               s_03a, s_13a, s_05a, s_15a, s_25a, &
               s_03b, s_13b, s_05b, s_15b, s_25b, &
               s_03, s_13, s_05, s_15, s_25, &
               m4p, m8p, m4pn, mn4pn, a2m4pn, a2m8p, &
               txbx, txby, txbz, &
               dxbx, dxby, dxbz, &
               dxbdt, dmdxx, dmdyy, dmdzz, dmdxy, dmdyz, dmdxz, &
               tmtxx, tmtyy, tmtzz, tmtxy, tmtyz, tmtxz, &
               tmdxx, tmdyy, tmdzz, tmdxy, tmdyz, tmdxz, &
               tmtxbxx, tmtxbyy, tmtxbzz, tmtxbxy, tmtxbyz, tmtxbxz, &
               dmtxbxx, dmtxbyy, dmtxbzz, dmtxbxy, dmtxbyz, dmtxbxz, &
               tmdxbxx, tmdxbyy, tmdxbzz, tmdxbxy, tmdxbyz, tmdxbxz, &
               I_03xx, I_03yy, I_03zz, I_03xy, I_03yz, I_03xz, &
               I_13xx, I_13yy, I_13zz, I_13xy, I_13yz, I_13xz, &
               I_05xx, I_05yy, I_05zz, I_05xy, I_05yz, I_05xz, &
               I_15xx, I_15yy, I_15zz, I_15xy, I_15yz, I_15xz, &
               I_25xx, I_25yy, I_25zz, I_25xy, I_25yz, I_25xz


        vec1x = p2x - p1x
        vec1y = p2y - p1y
        vec1z = p2z - p1z
    
        oneoverLp = 1.d0 / sqrt(vec1x*vec1x + vec1y*vec1y + vec1z*vec1z)
    
        tpx = vec1x * oneoverLp
        tpy = vec1y * oneoverLp
        tpz = vec1z * oneoverLp
        
        Rx = px - p1x
        Ry = py - p1y
        Rz = pz - p1z
        
        Rdt = Rx*tpx + Ry*tpy + Rz*tpz
        
        ndx = Rx - Rdt*tpx
        ndy = Ry - Rdt*tpy
        ndz = Rz - Rdt*tpz

        d2 = ndx*ndx + ndy*ndy + ndz*ndz
        
        s1 = -Rdt
        s2 = -((px-p2x)*tpx + (py-p2y)*tpy + (pz-p2z)*tpz)
        a2 = a * a
        a2_d2 = a2 + d2
        a2d2inv = 1.d0 / a2_d2
        
        Ra = sqrt(a2_d2 + s1*s1)
        Rainv = 1.d0 / Ra
        Ra3inv = Rainv * Rainv * Rainv
        sRa3inv = s1 * Ra3inv
        
        s_03a = s1 * Rainv * a2d2inv
        s_13a = -Rainv
        s_05a = (2.d0*s_03a + sRa3inv) * a2d2inv
        s_15a = -Ra3inv
        s_25a = s_03a - sRa3inv
        
        Ra = sqrt(a2_d2 + s2*s2)
        Rainv = 1.d0 / Ra
        Ra3inv = Rainv * Rainv * Rainv
        sRa3inv = s2 * Ra3inv
        
        s_03b = s2 * Rainv * a2d2inv
        s_13b = -Rainv
        s_05b = (2.d0*s_03b + sRa3inv) * a2d2inv
        s_15b = -Ra3inv
        s_25b = s_03b - sRa3inv
        
        s_03 = s_03b - s_03a
        s_13 = s_13b - s_13a
        s_05 = s_05b - s_05a
        s_15 = s_15b - s_15a
        s_25 = s_25b - s_25a
        
        m4p = 0.25d0 * MU / pivalue
        m8p = 0.5d0 * m4p
        m4pn = m4p / (1.d0 - NU)
        mn4pn = m4pn * NU
        a2m4pn = a2 * m4pn
        a2m8p = a2 * m8p
    
        
        txbx = tpy*bz - tpz*by
        txby = tpz*bx - tpx*bz
        txbz = tpx*by - tpy*bx
        
        dxbx = ndy*bz - ndz*by
        dxby = ndz*bx - ndx*bz
        dxbz = ndx*by - ndy*bx

        dxbdt = dxbx*tpx + dxby*tpy + dxbz*tpz
             
        dmdxx = ndx * ndx
        dmdyy = ndy * ndy
        dmdzz = ndz * ndz
        dmdxy = ndx * ndy
        dmdyz = ndy * ndz
        dmdxz = ndx * ndz
        
        tmtxx = tpx * tpx
        tmtyy = tpy * tpy
        tmtzz = tpz * tpz
        tmtxy = tpx * tpy
        tmtyz = tpy * tpz
        tmtxz = tpx * tpz
        
        tmdxx = 2.d0 * tpx * ndx
        tmdyy = 2.d0 * tpy * ndy
        tmdzz = 2.d0 * tpz * ndz
        tmdxy = tpx*ndy + tpy*ndx
        tmdyz = tpy*ndz + tpz*ndy
        tmdxz = tpx*ndz + tpz*ndx
         

        tmtxbxx = 2.d0 * tpx * txbx
        tmtxbyy = 2.d0 * tpy * txby
        tmtxbzz = 2.d0 * tpz * txbz
        tmtxbxy = tpx*txby + tpy*txbx
        tmtxbyz = tpy*txbz + tpz*txby
        tmtxbxz = tpx*txbz + tpz*txbx
        
        dmtxbxx = 2.d0 * ndx * txbx
        dmtxbyy = 2.d0 * ndy * txby
        dmtxbzz = 2.d0 * ndz * txbz
        dmtxbxy = ndx*txby + ndy*txbx
        dmtxbyz = ndy*txbz + ndz*txby
        dmtxbxz = ndx*txbz + ndz*txbx
        

        tmdxbxx = 2.d0 * tpx * dxbx
        tmdxbyy = 2.d0 * tpy * dxby
        tmdxbzz = 2.d0 * tpz * dxbz
        tmdxbxy = tpx*dxby + tpy*dxbx
        tmdxbyz = tpy*dxbz + tpz*dxby
        tmdxbxz = tpx*dxbz + tpz*dxbx
        
        common0 = m4pn * dxbdt
        
        I_03xx = common0 + m4pn*dmtxbxx - m4p*tmdxbxx
        I_03yy = common0 + m4pn*dmtxbyy - m4p*tmdxbyy
        I_03zz = common0 + m4pn*dmtxbzz - m4p*tmdxbzz
        I_03xy = m4pn*dmtxbxy - m4p*tmdxbxy
        I_03yz = m4pn*dmtxbyz - m4p*tmdxbyz
        I_03xz = m4pn*dmtxbxz - m4p*tmdxbxz
        
        I_13xx = -mn4pn * tmtxbxx
        I_13yy = -mn4pn * tmtxbyy
        I_13zz = -mn4pn * tmtxbzz
        I_13xy = -mn4pn * tmtxbxy
        I_13yz = -mn4pn * tmtxbyz
        I_13xz = -mn4pn * tmtxbxz

        I_05xx = common0*(a2+dmdxx) - a2m8p*tmdxbxx
        I_05yy = common0*(a2+dmdyy) - a2m8p*tmdxbyy
        I_05zz = common0*(a2+dmdzz) - a2m8p*tmdxbzz
        I_05xy = common0*dmdxy - a2m8p*tmdxbxy
        I_05yz = common0*dmdyz - a2m8p*tmdxbyz
        I_05xz = common0*dmdxz - a2m8p*tmdxbxz
        
        I_15xx = a2m8p*tmtxbxx - common0*tmdxx
        I_15yy = a2m8p*tmtxbyy - common0*tmdyy
        I_15zz = a2m8p*tmtxbzz - common0*tmdzz
        I_15xy = a2m8p*tmtxbxy - common0*tmdxy
        I_15yz = a2m8p*tmtxbyz - common0*tmdyz
        I_15xz = a2m8p*tmtxbxz - common0*tmdxz
        
        I_25xx = common0 * tmtxx
        I_25yy = common0 * tmtyy
        I_25zz = common0 * tmtzz
        I_25xy = common0 * tmtxy
        I_25yz = common0 * tmtyz
        I_25xz = common0 * tmtxz
        
        stress(1,1) = I_03xx*s_03 + I_13xx*s_13 + I_05xx*s_05 + &
                    I_15xx*s_15 + I_25xx*s_25

        stress(2,2) = I_03yy*s_03 + I_13yy*s_13 + I_05yy*s_05 + &
                    I_15yy*s_15 + I_25yy*s_25

        stress(3,3) = I_03zz*s_03 + I_13zz*s_13 + I_05zz*s_05 + &
                    I_15zz*s_15 + I_25zz*s_25

        stress(1,2) = I_03xy*s_03 + I_13xy*s_13 + I_05xy*s_05 + &
                    I_15xy*s_15 + I_25xy*s_25

        stress(2,3) = I_03yz*s_03 + I_13yz*s_13 + I_05yz*s_05 + &
                    I_15yz*s_15 + I_25yz*s_25

        stress(1,3) = I_03xz*s_03 + I_13xz*s_13 + I_05xz*s_05 + &
                    I_15xz*s_15 + I_25xz*s_25
                    
        stress(2,1) = stress(1,2)
        stress(3,2) = stress(2,3)
        stress(3,1) = stress(1,3)

        end subroutine StressDueToSeg
    ! ###################   




    ! ###################   
        subroutine SelfForceMatlab(p1, p2, f1, f2)

        use param, only: bv, MU, NU, a
        use utils
        implicit none

        real(kind=8)    ::  a0, Ec, p1(3), p2(3), f1(3), f2(3), &
                            rnd(3), S, Score, Stot, Lseg, Linv, La, Lainv, bdott, bdott2, bev2, omninv, LTcore, &
                            t(3), bev(3), fself(3)

        intent(in)      ::  p1, p2
        intent(out)     ::  f1, f2

        rnd    = p2 - p1
        Lseg   = norm(rnd)
        Linv   = 1.d0/Lseg
        La     = sqrt(Lseg*Lseg + a*a)
        Lainv  = 1.d0/La        
        t      = rnd*Linv       
        omninv = 1.d0/(1.d0-NU)
        bdott  = vdot(bv, t)
        bdott2 = bdott**2

        bev = bv - bdott*t
        bev2 = vdot(bev,bev)
    
        a0 = a/500.d0
        Ec = MU/(4.d0*pivalue)*log(a/a0)

        ! Elastic Self Interaction Force - Torsional Contribution
        S = (0.25d0*MU/pivalue)*bdott*( (NU*omninv)*( log((La+Lseg)/a) - 2.d0*(La-a)*Linv ) - 0.5d0*Linv*Lainv*(La-a)**2 )
        ! Core Self Interaction Force - Torsional Contribution
        Score = 2.d0*NU*omninv*Ec*bdott;
        Stot  = S + Score
        fself = Stot*bev
    
        ! Core Self Interaction Force - Longitudinal Component
        LTcore = (bdott2 + bev2*omninv)*Ec
        fself = fself - LTcore*t

        f2 = fself
        f1 = -fself

        end subroutine SelfForceMatlab
    ! ###################   


    ! ###################    	
	    subroutine segforcevec(rn, links, disl, appliedstress, fn)
	
        use param
        use utils
	    implicit none
	
	    real(kind=8)	::  sigb(3), rn(Nntot,3), appliedstress(3,3), &
    					    fn(Nntot,3), rnd(3), f0(Nntot-1,3), f1(Nntot-1,3), &
                            p1x, p1y, p1z, p2x, p2y, p2z, p3x, p3y, p3z, p4x, p4y, p4z, bpx, bpy ,bpz, bx, by, bz, &
                            fp1x, fp1y, fp1z, fp2x, fp2y, fp2z, fp3x, fp3y, fp3z, fp4x, fp4y, fp4z, &
                            fp1self(3), fp2self(3)
        logical         ::  seg12Local, seg34Local
	    integer 		::  links(Nntot-1,2), disl(Ndtot,2), n1, n2, n3, n4, i, j
	
	    intent(in)		::  rn, links, disl, appliedstress
	    intent(out)		:: 	fn
	
        fn    = 0.d0
        f0  = 0.d0
        f1  = 0.d0
        
	    sigb = mdot(appliedstress, bv)
    
        ! loop over all segments
	    do i=1, Nnodes-1
		    n1 = links(i,1)
		    n2 = links(i,2)
		    rnd = rn(n2,:) - rn(n1,:)
        
            ! caluclate force due to externally applied stress
		    fn(n1,:) = fn(n1,:) + 0.5d0*cross(sigb, rnd)
            fn(n2,:) = fn(n2,:) + 0.5d0*cross(sigb, rnd)
		
            ! calculate force due to the self-energy
            call SelfForceMatlab(rn(n1,:), rn(n2,:), fp1self, fp2self)

            fn(n2,:) = fn(n2,:) + fp2self
            fn(n1,:) = fn(n1,:) + fp1self
        
            !  remote forces
            do j=i+1, Nnodes-1
                n3 = links(j,1)
		        n4 = links(j,2)
                p1x = rn(n1,1)
                p1y = rn(n1,2)
                p1z = rn(n1,3)
                p2x = rn(n2,1)
                p2y = rn(n2,2)
                p2z = rn(n2,3)
                p3x = rn(n3,1)
                p3y = rn(n3,2)
                p3z = rn(n3,3)
                p4x = rn(n4,1)
                p4y = rn(n4,2)
                p4z = rn(n4,3)
                bpx = bv(1)
                bpy = bv(2)
                bpz = bv(3)
                bx  = bv(1)
                by  = bv(2)
                bz  = bv(3)
                seg12Local = .true.
                seg34Local = .true.
                fp1x = 0.d0; fp1y = 0.d0; fp1z = 0.d0; fp2x = 0.d0; fp2y = 0.d0; fp2z = 0.d0
                fp3x = 0.d0; fp3y = 0.d0; fp3z = 0.d0; fp4x = 0.d0; fp4y = 0.d0; fp4z = 0.d0
            
                call SegSegForce(p1x, p1y, p1z, p2x, p2y, p2z, p3x, p3y, p3z, p4x, p4y, p4z, &
                                bpx, bpy, bpz, bx, by, bz, &
                                seg12Local, seg34Local, &
                                fp1x, fp1y, fp1z, fp2x, fp2y, fp2z, fp3x, fp3y, fp3z, fp4x, fp4y, fp4z)
                
                fn(n1,1) = fn(n1,1) + fp1x
                fn(n1,2) = fn(n1,2) + fp1y
                fn(n1,3) = fn(n1,3) + fp1z
                
                fn(n3,1) = fn(n3,1) + fp3x
                fn(n3,2) = fn(n3,2) + fp3y
                fn(n3,3) = fn(n3,3) + fp3z
                
                fn(n2,1) = fn(n2,1) + fp2x
                fn(n2,2) = fn(n2,2) + fp2y
                fn(n2,3) = fn(n2,3) + fp2z
                
                fn(n4,1) = fn(n4,1) + fp4x
                fn(n4,2) = fn(n4,2) + fp4y
                fn(n4,3) = fn(n4,3) + fp4z
 !               
                f0(i,1)=f0(i,1)+fp1x
                f0(i,2)=f0(i,2)+fp1y
                f0(i,3)=f0(i,3)+fp1z
                
                f0(j,1)=f0(j,1)+fp3x
                f0(j,2)=f0(j,2)+fp3y
                f0(j,3)=f0(j,3)+fp3z
                
                f1(i,1)=f1(i,1)+fp2x
                f1(i,2)=f1(i,2)+fp2y
                f1(i,3)=f1(i,3)+fp2z
                
                f1(j,1)=f1(j,1)+fp4x
                f1(j,2)=f1(j,2)+fp4y
                f1(j,3)=f1(j,3)+fp4z
            end do
        end do 
	
	    end subroutine segforcevec
    ! ###################    		
   

    ! ###################  
        !/*-------------------------------------------------------------------------
        ! *
        ! *      Function:     SpecialSegSegForceIntegrals
        ! *      Description:  Calculates the integrals required for the
        ! *                    force calculation for near-parallel dislocation
        ! *                    segment pairs.
        ! *
        ! *-----------------------------------------------------------------------*/
        subroutine SpecialSegSegForceIntegrals(a2, d2, yin, zin, &
                                                f_003,  f_103, f_013, &
                                                f_113, f_213, f_123, &
                                                f_005, f_105, f_015, &
                                                f_115, f_215, f_125)
        
        use utils
        implicit none
        real(kind=8)    :: a2, d2, yin, zin, &
                            f_003,  f_103, f_013, &
                            f_113, f_213, f_123, &
                            f_005, f_105, f_015, &
                            f_115, f_215, f_125
        intent(in)      :: a2, d2, yin, zin
        intent(out)     :: f_003,  f_103, f_013, &
                            f_113, f_213, f_123, &
                            f_005, f_105, f_015, &
                            f_115, f_215, f_125
        real(kind=8)    :: a2_d2, a2d2inv, ypz, ymz, Ra, Rainv, Log_Ra_ypz, common1

        a2_d2 = a2 + d2   
        a2d2inv = 1.d0 / a2_d2
        ypz = yin + zin
        ymz = yin - zin
        Ra = sqrt(a2_d2 + ypz*ypz)
        Rainv = 1.d0 / Ra
        Log_Ra_ypz = log(Ra + ypz)
        
        common1 = ymz * Ra * a2d2inv

        f_003 = Ra * a2d2inv
        f_103 = -0.5d0 * (Log_Ra_ypz - common1)
        f_013 = -0.5d0 * (Log_Ra_ypz + common1)
        f_113 = -Log_Ra_ypz
        f_213 = zin*Log_Ra_ypz - Ra
        f_123 = yin*Log_Ra_ypz - Ra
        
        f_005 =  a2d2inv * (2*a2d2inv*Ra - Rainv)
        f_105 =  a2d2inv * (common1 - yin*Rainv)
        f_015 = -a2d2inv * (common1 + zin*Rainv)
        f_115 = -a2d2inv * ypz * Rainv
        f_215 =  Rainv - zin * f_115
        f_125 =  Rainv - yin * f_115

        end subroutine SpecialSegSegForceIntegrals


        ! *-------------------------------------------------------------------------
        ! *
        ! *      Function:     SegSegForceIntegrals
        ! *      Description:  Calculates the integrals required for the
        ! *                    force calculation
        ! *
        ! *-------------------------------------------------------------------------
        subroutine SegSegForceIntegrals(d, c,yin,zin, &
                                        f_003, f_103, f_013, &
                                        f_113, f_203, f_023, &
                                        f_005, f_105, f_015, &
                                        f_115, f_205, f_025, &
                                        f_215, f_125, f_225, &
                                        f_305, f_035, f_315, &
                                        f_135)
        use param
        implicit none
        real(kind=8) :: d, c,yin,zin, &
                        f_003, f_103, f_013, &
                        f_113, f_203, f_023, &
                        f_005, f_105, f_015, &
                        f_115, f_205, f_025, &
                        f_215, f_125, f_225, &
                        f_305, f_035, f_315, &
                        f_135
        intent(in)   :: d, c,yin,zin
        intent(out)  :: f_003, f_103, f_013, &
                        f_113, f_203, f_023, &
                        f_005, f_105, f_015, &
                        f_115, f_205, f_025, &
                        f_215, f_125, f_225, &
                        f_305, f_035, f_315, &
                        f_135
        real(kind=8) :: c2, onemc2, onemc2inv, denom, &
                        a2_d2, y2, z2, Ra, &
                        Ra_Rdot_t, log_Ra_Rdot_t, zlog_Ra_Rdot_t, &
                        Ra_Rdot_tp, log_Ra_Rdot_tp, ylog_Ra_Rdot_tp, &
                        Rainv, Ra2_R_tinv, zRa2_R_tinv, z2Ra2_R_tinv, &
                        Ra2_R_tpinv, yRa2_R_tpinv, y2Ra2_R_tpinv, &
                        adf_003, tf_113, &
                        commonf025, commonf035, commonf205, &
                        commonf223, commonf225, commonf305, &
                        ycommonf025, zcommonf305, zcommonf205

        c2 = c*c
        onemc2 = 1.d0-c2      
        onemc2inv = 1.d0/onemc2         
        
        a2_d2 = a*a+d*d*onemc2
        y2    = yin*yin
        z2    = zin*zin
        Ra    = sqrt(a2_d2 + y2 + z2 + 2*yin*zin*c)        
        Rainv = 1.d0/Ra
        
        Ra_Rdot_tp = Ra+zin+yin*c       
        Ra_Rdot_t  = Ra+yin+zin*c       
         
        log_Ra_Rdot_tp =     log(Ra_Rdot_tp)
        ylog_Ra_Rdot_tp = yin*log_Ra_Rdot_tp
        
        log_Ra_Rdot_t =     log(Ra_Rdot_t)
        zlog_Ra_Rdot_t = zin*log_Ra_Rdot_t
        
        Ra2_R_tpinv = Rainv/Ra_Rdot_tp
        yRa2_R_tpinv = yin*  Ra2_R_tpinv
        y2Ra2_R_tpinv = yin* yRa2_R_tpinv
        
        Ra2_R_tinv = Rainv/Ra_Rdot_t
        zRa2_R_tinv = zin* Ra2_R_tinv
        z2Ra2_R_tinv = zin*zRa2_R_tinv

        denom = 1.d0/sqrt(onemc2*a2_d2)  
        
        f_003 = -2.d0*denom*atan((1+c)*(Ra+yin+zin)*denom)
        
        adf_003 = a2_d2*f_003
        commonf223 = (c*Ra - adf_003)*onemc2inv
        
        f_103 = (c*log_Ra_Rdot_t  - log_Ra_Rdot_tp)*onemc2inv
        f_013 = (c*log_Ra_Rdot_tp - log_Ra_Rdot_t )*onemc2inv
        f_113 = (c*adf_003 - Ra)*onemc2inv
        f_203 =  zlog_Ra_Rdot_t  + commonf223
        f_023 =  ylog_Ra_Rdot_tp + commonf223

        commonf225 = f_003 - c*Rainv
        commonf025 = c*yRa2_R_tpinv - Rainv
        ycommonf025 = yin*commonf025
        commonf205 = c*zRa2_R_tinv  - Rainv
        zcommonf205 = zin*commonf205
        commonf305 = log_Ra_Rdot_t  -(yin-c*zin)*Rainv - c2*z2Ra2_R_tinv
        zcommonf305 = zin*commonf305
        commonf035 = log_Ra_Rdot_tp -(zin-c*yin)*Rainv - c2*y2Ra2_R_tpinv

        tf_113 = 2.d0*f_113
        
        f_005 = (f_003 - yRa2_R_tpinv - zRa2_R_tinv)/(a2_d2)
        f_105 = (Ra2_R_tpinv - c*Ra2_R_tinv)*onemc2inv
        f_015 = (Ra2_R_tinv  - c*Ra2_R_tpinv)*onemc2inv
        f_115 = (Rainv - c*(yRa2_R_tpinv + zRa2_R_tinv + f_003))*onemc2inv
        f_205 = (yRa2_R_tpinv + c2*zRa2_R_tinv  + commonf225)*onemc2inv
        f_025 = (zRa2_R_tinv  + c2*yRa2_R_tpinv + commonf225)*onemc2inv
        f_215 = (f_013 - ycommonf025 + c*(zcommonf205-f_103))*onemc2inv 
        f_125 = (f_103 - zcommonf205 + c*(ycommonf025 - f_013))*onemc2inv 
        f_225 = (f_203 - zcommonf305 + c*(y2*commonf025 - tf_113))*onemc2inv
        f_305 = (y2Ra2_R_tpinv + c*commonf305 + 2*f_103)*onemc2inv
        f_035 = (z2Ra2_R_tinv  + c*commonf035 + 2*f_013)*onemc2inv
        f_315 = (tf_113 - y2*commonf025 + c*(zcommonf305 - f_203))*onemc2inv
        f_135 = (tf_113 - z2*commonf205 + c*(yin*commonf035-f_023))*onemc2inv
    
        end subroutine SegSegForceIntegrals


        !/*-------------------------------------------------------------------------
        ! *
        ! *      Function:     SpecialSegSegForce
        ! *      Description:  Special function for calculating forces between
        ! *                    dislocation segments too close to parallel to be
        ! *                    calculated via the function used for regular
        ! *                    segment/segment forces.
        ! *      Arguments:
        ! *          p1*,p2*      endpoints for dislocation segment beginning
        ! *                       at point p1 and ending at point p2
        ! *          p3*,p4*      endpoints for dislocation segment beginning
        ! *                       at point p3 and ending at point p4
        ! *          bpx,bpy,bpz  burgers vector for segment p1->p2
        ! *          bx,by,bz     burgers vector for segment p3->p4
        ! *          a            core parameter
        ! *          MU           shear modulus
        ! *          NU           poisson ratio
        ! *          seg12Local   1 if either node of segment p1->p2 is local to
        ! *                       the current domain, zero otherwise.
        ! *          seg34Local   1 if either node of segment p3->p4 is local to
        ! *                       the current domain, zero otherwise.
        ! *          fp1*, fp2*,  pointers to locations in which to return forces
        ! *          fp3*, fp4*   on nodes p1 thru p4 respectively
        ! *
        ! *-----------------------------------------------------------------------*/
        subroutine SpecialSegSegForce(p1x, p1y, p1z, &
                                        p2x, p2y, p2z, &
                                        p3x, p3y, p3z, &
                                        p4x, p4y, p4z, &
                                        bpx, bpy, bpz, &
                                        bx, by, bz, &
                                        ecrit, &
                                        seg12Local, seg34Local, &
                                        fp1x, fp1y, fp1z, &
                                        fp2x, fp2y, fp2z, &
                                        fp3x, fp3y, fp3z, &
                                        fp4x, fp4y, fp4z)
        use param
        use utils
        implicit none
        real(kind=8) :: p1x, p1y, p1z, &
                        p2x, p2y, p2z, &
                        p3x, p3y, p3z, &
                        p4x, p4y, p4z, &
                        bpx, bpy, bpz, &
                        bx, by, bz, &
                        ecrit, &
                        fp1x, fp1y, fp1z, &
                        fp2x, fp2y, fp2z, &
                        fp3x, fp3y, fp3z, &
                        fp4x, fp4y, fp4z
        logical      :: seg12Local, seg34Local
        intent(inout):: p1x, p1y, p1z, &
                        p2x, p2y, p2z, &
                        p3x, p3y, p3z, &
                        p4x, p4y, p4z, &
                        bpx, bpy, bpz, &
                        bx, by, bz
        intent(in)   :: ecrit, seg12Local, seg34Local
        intent(out)  :: fp1x, fp1y, fp1z, &
                        fp2x, fp2y, fp2z, &
                        fp3x, fp3y, fp3z, &
                        fp4x, fp4y, fp4z
        real(kind=8) :: eps, c, a2, d2, &
                        Rx, Ry, Rz, Rdt, Rdtp, &
                        oneoverL, oneoverLp, &
                        temp, tempx, tempy, tempz, tempx2, tempy2, tempz2, &
                        common1, common4, &
                        common2x, common2y, common2z, &
                        common3x, common3y, common3z, &
                        p1modx, p1mody, p1modz, &
                        p2modx, p2mody, p2modz, &
                        p3modx, p3mody, p3modz, &
                        p4modx, p4mody, p4modz, &
                        vec1x, vec1y, vec1z, &
                        tx, ty, tz, &
                        tpx, tpy, tpz, &
                        diffx, diffy, diffz, magdiff, &
                        ndx, ndy, ndz, &
                        wx, wy, wz, &
                        qx, qy, qz, &
                        ya, yb, za, zb, &
                        fp1xcor, fp1ycor, fp1zcor, &
                        fp2xcor, fp2ycor, fp2zcor, &
                        fp3xcor, fp3ycor, fp3zcor, &
                        fp4xcor, fp4ycor, fp4zcor, &
                        f_003,  f_103,  f_013,  f_113,  f_213,  f_123,  f_005,  f_105, &
                        f_003a, f_103a, f_013a, f_113a, f_213a, f_123a, f_005a, f_105a, &
                        f_015,  f_115,  f_215,  f_125, &
                        f_015a, f_115a, f_215a, f_125a, &
                        Fint_003, Fint_113, Fint_005, Fint_115, &
                        I_003x, I_003y, I_003z, &
                        I_113x, I_113y, I_113z, &
                        I_005x, I_005y, I_005z, &
                        I_115x, I_115y, I_115z, &
                        m4p, m8p, m4pn, a2m4pn, a2m8p, &
                        tdb, tdbp, nddb, &
                        bctx, bcty, bctz, &
                        bpctx, bpcty, bpctz, &
                        ndctx, ndcty, ndctz, &
                        bpctdb, bpctdnd, &
                        bpctctx, bpctcty, bpctctz, &
                        tpdb, tpdbp, nddbp, &
                        bctpx, bctpy, bctpz, &
                        bpctpx, bpctpy, bpctpz, &
                        ndctpx, ndctpy, ndctpz, &
                        bctpdbp, bctpdnd, &
                        bctpctpx, bctpctpy, bctpctpz, &
                        diffMag2, p1modMag2, p2modMag2, p3modMag2, p4modMag2, &
                        cotanthetac
        logical :: flip = .FALSE.

        cotanthetac = sqrt((1.d0 - ecrit*1.01d0) / (ecrit*1.01d0))

        eps    = 1.d-16
        a2     = a*a
        m4p    = 0.25d0 * MU / pivalue
        m8p    = 0.5d0 * m4p
        m4pn   = m4p / ( 1.d0 - NU )
        a2m4pn = a2 * m4pn
        a2m8p  = a2 * m8p

        fp1x = 0.d0
        fp1y = 0.d0
        fp1z = 0.d0
            
        fp2x = 0.d0
        fp2y = 0.d0
        fp2z = 0.d0
           
        fp3x = 0.d0
        fp3y = 0.d0
        fp3z = 0.d0
        
        fp4x = 0.d0
        fp4y = 0.d0
        fp4z = 0.d0
        
        vec1x = p4x - p3x
        vec1y = p4y - p3y
        vec1z = p4z - p3z
           
        oneoverL = 1.d0/sqrt(vec1x*vec1x + vec1y*vec1y + vec1z*vec1z)
        
        tx = vec1x*oneoverL
        ty = vec1y*oneoverL
        tz = vec1z*oneoverL
        
        vec1x = p2x - p1x
        vec1y = p2y - p1y
        vec1z = p2z - p1z
        
        oneoverLp = 1.d0/sqrt(vec1x*vec1x + vec1y*vec1y + vec1z*vec1z)
        
        tpx = vec1x*oneoverLp
        tpy = vec1y*oneoverLp
        tpz = vec1z*oneoverLp
        
        c = tx*tpx + ty*tpy + tz*tpz 
                    
        if (c .LT. 0.d0) then
            flip = .TRUE.
            tempx = p2x
            tempy = p2y
            tempz = p2z
            p2x = p1x
            p2y = p1y
            p2z = p1z
            p1x = tempx
            p1y = tempy
            p1z = tempz
            tpx = -tpx
            tpy = -tpy
            tpz = -tpz
            bpx = -bpx
            bpy = -bpy
            bpz = -bpz
        end if 
            
!/*
! *      Find f3 and f4, but only if at least one of the segment
! *      endpoints is local to the domain.
! */
        if (seg34Local) then
            temp = (p2x-p1x)*tx + (p2y-p1y)*ty + (p2z-p1z)*tz
           
            p2modx = p1x + temp*tx
            p2mody = p1y + temp*ty
            p2modz = p1z + temp*tz
            
            diffx = p2x - p2modx
            diffy = p2y - p2mody
            diffz = p2z - p2modz
            
            magdiff = sqrt(diffx*diffx + diffy*diffy + diffz*diffz)

            tempx2 = (0.5d0 * cotanthetac) * magdiff * tx
            tempy2 = (0.5d0 * cotanthetac) * magdiff * ty
            tempz2 = (0.5d0 * cotanthetac) * magdiff * tz

            p1modx = p1x + 0.5d0*diffx + tempx2
            p1mody = p1y + 0.5d0*diffy + tempy2
            p1modz = p1z + 0.5d0*diffz + tempz2

            p2modx = p2modx + 0.5d0*diffx - tempx2
            p2mody = p2mody + 0.5d0*diffy - tempy2
            p2modz = p2modz + 0.5d0*diffz - tempz2
            
            Rx = p3x - p1modx
            Ry = p3y - p1mody
            Rz = p3z - p1modz
            
            Rdt = Rx*tx + Ry*ty + Rz*tz
            
            ndx = Rx - Rdt*tx
            ndy = Ry - Rdt*ty
            ndz = Rz - Rdt*tz
            
            d2 = ndx*ndx + ndy*ndy + ndz*ndz
            
            ya = p3x*tx + p3y*ty + p3z*tz
            yb = p4x*tx + p4y*ty + p4z*tz
            za = -(p1modx*tx + p1mody*ty + p1modz*tz)
            zb = -(p2modx*tx + p2mody*ty + p2modz*tz)
            
            call SpecialSegSegForceIntegrals(a2, d2, ya, za, &
                                        f_003, f_103, f_013, f_113, &
                                        f_213, f_123, f_005, f_105, &
                                        f_015, f_115, f_215, f_125)
        
            call SpecialSegSegForceIntegrals(a2, d2, ya, zb, &
                                        f_003a, f_103a, f_013a, f_113a, &
                                        f_213a, f_123a, f_005a, f_105a, &
                                        f_015a, f_115a, f_215a, f_125a)

            f_003 = f_003 - f_003a
            f_103 = f_103 - f_103a
            f_013 = f_013 - f_013a
            f_113 = f_113 - f_113a
            f_213 = f_213 - f_213a
            f_123 = f_123 - f_123a
            f_005 = f_005 - f_005a
            f_105 = f_105 - f_105a
            f_015 = f_015 - f_015a
            f_115 = f_115 - f_115a
            f_215 = f_215 - f_215a
            f_125 = f_125 - f_125a
        
            call SpecialSegSegForceIntegrals(a2, d2, yb, za, &
                                        f_003a, f_103a, f_013a, f_113a, &
                                        f_213a, f_123a, f_005a, f_105a, &
                                        f_015a, f_115a, f_215a, f_125a)
        
            f_003 = f_003 - f_003a
            f_103 = f_103 - f_103a
            f_013 = f_013 - f_013a
            f_113 = f_113 - f_113a
            f_213 = f_213 - f_213a
            f_123 = f_123 - f_123a
            f_005 = f_005 - f_005a
            f_105 = f_105 - f_105a
            f_015 = f_015 - f_015a
            f_115 = f_115 - f_115a
            f_215 = f_215 - f_215a
            f_125 = f_125 - f_125a
        
            call SpecialSegSegForceIntegrals(a2, d2, yb, zb, &
                                        f_003a, f_103a, f_013a, f_113a, &
                                        f_213a, f_123a, f_005a, f_105a, &
                                        f_015a, f_115a, f_215a, f_125a)
        
            f_003 = f_003 + f_003a
            f_103 = f_103 + f_103a
            f_013 = f_013 + f_013a
            f_113 = f_113 + f_113a
            f_213 = f_213 + f_213a
            f_123 = f_123 + f_123a
            f_005 = f_005 + f_005a
            f_105 = f_105 + f_105a
            f_015 = f_015 + f_015a
            f_115 = f_115 + f_115a
            f_215 = f_215 + f_215a
            f_125 = f_125 + f_125a
            
            tdb = tx*bx + ty*by + tz*bz
        
            tdbp = tx*bpx + ty*bpy + tz*bpz
        
            nddb = ndx*bx + ndy*by + ndz*bz
            
            bctx = by*tz - bz*ty
            bcty = bz*tx - bx*tz 
            bctz = bx*ty - by*tx
            
            bpctx = bpy*tz - bpz*ty
            bpcty = bpz*tx - bpx*tz 
            bpctz = bpx*ty - bpy*tx
            
            ndctx = ndy*tz - ndz*ty
            ndcty = ndz*tx - ndx*tz 
            ndctz = ndx*ty - ndy*tx
            
            bpctdb = bpctx*bx + bpcty*by + bpctz*bz
            bpctdnd = bpctx*ndx + bpcty*ndy + bpctz*ndz
            bpctctx = tdbp*tx - bpx
            bpctcty = tdbp*ty - bpy
            bpctctz = tdbp*tz - bpz
            
            common1 = tdb*tdbp
            
            common2x = common1*ndx
            common2y = common1*ndy
            common2z = common1*ndz
            
            common3x = bpctdnd*bctx
            common3y = bpctdnd*bcty
            common3z = bpctdnd*bctz
            
            I_003x = m4pn*(nddb*bpctctx + bpctdb*ndctx - common3x) - &
                        m4p*common2x 
            I_003y = m4pn*(nddb*bpctcty + bpctdb*ndcty - common3y) - &
                        m4p*common2y 
            I_003z = m4pn*(nddb*bpctctz + bpctdb*ndctz - common3z) - &
                        m4p*common2z 
            
            common4 = (m4pn-m4p)*tdb
            
            I_113x =  common4*bpctctx
            I_113y =  common4*bpctcty
            I_113z =  common4*bpctctz
            
            common1 = m4pn*bpctdnd*nddb
            
            I_005x = -a2m8p*common2x - a2m4pn*common3x - common1*ndctx
            I_005y = -a2m8p*common2y - a2m4pn*common3y - common1*ndcty
            I_005z = -a2m8p*common2z - a2m4pn*common3z - common1*ndctz
            
            common1 = a2m8p*tdb
            common4 = m4pn*bpctdnd*tdb
            
            I_115x = -common1*bpctctx - common4*ndctx
            I_115y = -common1*bpctcty - common4*ndcty
            I_115z = -common1*bpctctz - common4*ndctz

            Fint_003 = f_103 - ya*f_003
            Fint_113 = f_213 - ya*f_113
            Fint_005 = f_105 - ya*f_005
            Fint_115 = f_215 - ya*f_115
             
            fp4x = (I_003x*Fint_003 + I_113x*Fint_113 + I_005x*Fint_005 + &
                        I_115x*Fint_115) * oneoverL
            fp4y = (I_003y*Fint_003 + I_113y*Fint_113 + I_005y*Fint_005 + &
                        I_115y*Fint_115) * oneoverL
            fp4z = (I_003z*Fint_003 + I_113z*Fint_113 + I_005z*Fint_005 + &
                        I_115z*Fint_115) * oneoverL
             
            Fint_003 = yb*f_003 - f_103
            Fint_113 = yb*f_113 - f_213
            Fint_005 = yb*f_005 - f_105
            Fint_115 = yb*f_115 - f_215
             
            fp3x = (I_003x*Fint_003 + I_113x*Fint_113 + I_005x*Fint_005 + &
                        I_115x*Fint_115) * oneoverL
            fp3y = (I_003y*Fint_003 + I_113y*Fint_113 + I_005y*Fint_005 + &
                        I_115y*Fint_115) * oneoverL
            fp3z = (I_003z*Fint_003 + I_113z*Fint_113 + I_005z*Fint_005 + &
                        I_115z*Fint_115) * oneoverL
             

            diffMag2 = (diffx*diffx + diffy*diffy + diffz*diffz)
            p1modMag2 = (p1modx*p1modx + p1mody*p1mody + p1modz*p1modz)
            p2modMag2 = (p2modx*p2modx + p2mody*p2mody + p2modz*p2modz)

            if (diffMag2 .GT. (eps * (p1modMag2+p2modMag2))) then
        
                call SegSegForce(p1x, p1y, p1z, p1modx, p1mody, p1modz, &
                            p3x, p3y, p3z, p4x, p4y, p4z, &
                            bpx, bpy, bpz, bx, by, bz, &
                            seg12Local, seg34Local, &
                            wx, wy, wz, qx, qy, qz, &
                            fp3xcor, fp3ycor, fp3zcor, &
                            fp4xcor, fp4ycor, fp4zcor)
        
                fp3x = fp3x + fp3xcor
                fp3y = fp3y + fp3ycor
                fp3z = fp3z + fp3zcor
                fp4x = fp4x + fp4xcor
                fp4y = fp4y + fp4ycor
                fp4z = fp4z + fp4zcor
        
                call SegSegForce(p2modx, p2mody, p2modz, p2x, p2y, p2z, &
                            p3x, p3y, p3z, p4x, p4y, p4z, &
                            bpx, bpy, bpz, bx, by, bz, &
                            seg12Local, seg34Local, &
                            wx, wy, wz, qx, qy, qz, &
                            fp3xcor, fp3ycor, fp3zcor, &
                            fp4xcor, fp4ycor, fp4zcor)
        
                fp3x = fp3x + fp3xcor
                fp3y = fp3y + fp3ycor
                fp3z = fp3z + fp3zcor
                fp4x = fp4x + fp4xcor
                fp4y = fp4y + fp4ycor
                fp4z = fp4z + fp4zcor
            end if
        end if
         ! if segment p3->p4 is local */
             
        !/*
        !*      Find f1 and f2, but only if at least one of the endpoints
        !*      is local to the domain.
        !*/
        if (seg12Local) then
            temp = (p4x-p3x)*tpx + (p4y-p3y)*tpy + (p4z-p3z)*tpz
             
            p4modx = p3x + temp*tpx
            p4mody = p3y + temp*tpy
            p4modz = p3z + temp*tpz
             
            diffx = p4x - p4modx
            diffy = p4y - p4mody
            diffz = p4z - p4modz
             
            magdiff = sqrt(diffx*diffx + diffy*diffy + diffz*diffz)

            tempx2 = (0.5d0 * cotanthetac) * magdiff * tpx
            tempy2 = (0.5d0 * cotanthetac) * magdiff * tpy
            tempz2 = (0.5d0 * cotanthetac) * magdiff * tpz

            p3modx = p3x + 0.5d0*diffx + tempx2
            p3mody = p3y + 0.5d0*diffy + tempy2
            p3modz = p3z + 0.5d0*diffz + tempz2
             
            p4modx = p4modx + 0.5d0*diffx - tempx2
            p4mody = p4mody + 0.5d0*diffy - tempy2
            p4modz = p4modz + 0.5d0*diffz - tempz2
             
            Rx = (p3modx - p1x)
            Ry = (p3mody - p1y)
            Rz = (p3modz - p1z)
             
            Rdtp = Rx*tpx + Ry*tpy + Rz*tpz
             
            ndx = Rx - Rdtp*tpx
            ndy = Ry - Rdtp*tpy
            ndz = Rz - Rdtp*tpz
             
            d2 = ndx*ndx + ndy*ndy + ndz*ndz
             
            yb = p4modx*tpx + p4mody*tpy + p4modz*tpz
            ya = p3modx*tpx + p3mody*tpy + p3modz*tpz
            za = -(p1x*tpx + p1y*tpy + p1z*tpz)
            zb = -(p2x*tpx + p2y*tpy + p2z*tpz)
             
            call SpecialSegSegForceIntegrals(a2, d2, ya, za, &
                                        f_003, f_103, f_013, f_113, &
                                        f_213, f_123, f_005, f_105, &
                                        f_015, f_115, f_215, f_125)
        
            call SpecialSegSegForceIntegrals(a2, d2, ya, zb, &
                                        f_003a, f_103a, f_013a, f_113a, &
                                        f_213a, f_123a, f_005a, f_105a, &
                                        f_015a, f_115a, f_215a, f_125a)
        
            f_003 = f_003 - f_003a
            f_103 = f_103 - f_103a
            f_013 = f_013 - f_013a
            f_113 = f_113 - f_113a
            f_213 = f_213 - f_213a
            f_123 = f_123 - f_123a
            f_005 = f_005 - f_005a
            f_105 = f_105 - f_105a
            f_015 = f_015 - f_015a
            f_115 = f_115 - f_115a
            f_215 = f_215 - f_215a
            f_125 = f_125 - f_125a
        
            call SpecialSegSegForceIntegrals(a2, d2, yb, za, &
                                        f_003a, f_103a, f_013a, f_113a, &
                                        f_213a, f_123a, f_005a, f_105a, &
                                        f_015a, f_115a, f_215a, f_125a)
        
            f_003 = f_003 - f_003a
            f_103 = f_103 - f_103a
            f_013 = f_013 - f_013a
            f_113 = f_113 - f_113a
            f_213 = f_213 - f_213a
            f_123 = f_123 - f_123a
            f_005 = f_005 - f_005a
            f_105 = f_105 - f_105a
            f_015 = f_015 - f_015a
            f_115 = f_115 - f_115a
            f_215 = f_215 - f_215a
            f_125 = f_125 - f_125a
        
            call SpecialSegSegForceIntegrals(a2, d2, yb, zb, &
                                        f_003a, f_103a, f_013a, f_113a, &
                                        f_213a, f_123a, f_005a, f_105a, &
                                        f_015a, f_115a, f_215a, f_125a)
        
            f_003 = f_003 + f_003a
            f_103 = f_103 + f_103a
            f_013 = f_013 + f_013a
            f_113 = f_113 + f_113a
            f_213 = f_213 + f_213a
            f_123 = f_123 + f_123a
            f_005 = f_005 + f_005a
            f_105 = f_105 + f_105a
            f_015 = f_015 + f_015a
            f_115 = f_115 + f_115a
            f_215 = f_215 + f_215a
            f_125 = f_125 + f_125a
             
            tpdb = tpx*bx + tpy*by + tpz*bz
            tpdbp = tpx*bpx + tpy*bpy + tpz*bpz
            nddbp = ndx*bpx + ndy*bpy + ndz*bpz
        
            bctpx = by*tpz - bz*tpy 
            bctpy = bz*tpx - bx*tpz 
            bctpz = bx*tpy - by*tpx
            
            bpctpx = bpy*tpz - bpz*tpy 
            bpctpy = bpz*tpx - bpx*tpz 
            bpctpz = bpx*tpy - bpy*tpx
            
            ndctpx = ndy*tpz - ndz*tpy 
            ndctpy = ndz*tpx - ndx*tpz 
            ndctpz = ndx*tpy - ndy*tpx
            
            bctpdbp = bctpx*bpx + bctpy*bpy + bctpz*bpz
            bctpdnd = bctpx*ndx + bctpy*ndy + bctpz*ndz
            
            bctpctpx = tpdb*tpx - bx
            bctpctpy = tpdb*tpy - by
            bctpctpz = tpdb*tpz - bz
            
            common1 = tpdbp*tpdb
            
            common2x = common1*ndx
            common2y = common1*ndy
            common2z = common1*ndz
            
            common3x = bctpdnd*bpctpx
            common3y = bctpdnd*bpctpy
            common3z = bctpdnd*bpctpz
            
            I_003x = m4pn*(nddbp*bctpctpx+bctpdbp*ndctpx-common3x) - &
                        m4p*common2x 
            I_003y = m4pn*(nddbp*bctpctpy+bctpdbp*ndctpy-common3y) - &
                        m4p*common2y 
            I_003z = m4pn*(nddbp*bctpctpz+bctpdbp*ndctpz-common3z) - &
                        m4p*common2z 
            
            common1 = (m4pn-m4p)*tpdbp
            
            I_113x =  common1*bctpctpx
            I_113y =  common1*bctpctpy
            I_113z =  common1*bctpctpz
            
            common1 = m4pn*bctpdnd*nddbp
            
            I_005x = -a2m8p*common2x - a2m4pn*common3x - common1*ndctpx
            I_005y = -a2m8p*common2y - a2m4pn*common3y - common1*ndctpy
            I_005z = -a2m8p*common2z - a2m4pn*common3z - common1*ndctpz
            
            common1 = a2m8p*tpdbp
            common4 = m4pn*bctpdnd*tpdbp
            
            I_115x = -common1*bctpctpx - common4*ndctpx
            I_115y = -common1*bctpctpy - common4*ndctpy
            I_115z = -common1*bctpctpz - common4*ndctpz
            
            Fint_003 = f_013 - za*f_003
            Fint_113 = f_123 - za*f_113
            Fint_005 = f_015 - za*f_005
            Fint_115 = f_125 - za*f_115
             
            fp2x = (I_003x*Fint_003 + I_113x*Fint_113 + I_005x*Fint_005 + &
                        I_115x*Fint_115) * oneoverLp
            fp2y = (I_003y*Fint_003 + I_113y*Fint_113 + I_005y*Fint_005 + &
                        I_115y*Fint_115) * oneoverLp
            fp2z = (I_003z*Fint_003 + I_113z*Fint_113 + I_005z*Fint_005 + &
                        I_115z*Fint_115) * oneoverLp
             
            Fint_003 = zb*f_003 - f_013
            Fint_113 = zb*f_113 - f_123
            Fint_005 = zb*f_005 - f_015
            Fint_115 = zb*f_115 - f_125
             
            fp1x = (I_003x*Fint_003 + I_113x*Fint_113 + I_005x*Fint_005 + &
                        I_115x*Fint_115) * oneoverLp
            fp1y = (I_003y*Fint_003 + I_113y*Fint_113 + I_005y*Fint_005 + &
                        I_115y*Fint_115) * oneoverLp
            fp1z = (I_003z*Fint_003 + I_113z*Fint_113 + I_005z*Fint_005 + &
                        I_115z*Fint_115) * oneoverLp
             

            diffMag2 = (diffx*diffx + diffy*diffy + diffz*diffz)
            p3modMag2 = (p3modx*p3modx + p3mody*p3mody + p3modz*p3modz)
            p4modMag2 = (p4modx*p4modx + p4mody*p4mody + p4modz*p4modz)

            if (diffMag2 .GT. (eps * (p3modMag2+p4modMag2))) then
        
                call SegSegForce(p3x, p3y, p3z, p3modx, p3mody, p3modz, &
                            p1x, p1y, p1z, p2x, p2y, p2z, &
                            bx, by, bz, bpx, bpy, bpz, &
                            seg12Local, seg34Local, &
                            wx, wy, wz, qx, qy, qz, &
                            fp1xcor, fp1ycor, fp1zcor, &
                            fp2xcor, fp2ycor, fp2zcor)
        
                fp1x = fp1x + fp1xcor
                fp1y = fp1y + fp1ycor
                fp1z = fp1z + fp1zcor
                fp2x = fp2x + fp2xcor
                fp2y = fp2y + fp2ycor
                fp2z = fp2z + fp2zcor
        
                call SegSegForce(p4modx, p4mody, p4modz, p4x, p4y, p4z, &
                            p1x, p1y, p1z, p2x, p2y, p2z, &
                            bx, by, bz, bpx, bpy, bpz, &
                            seg12Local, seg34Local, &
                            wx, wy, wz, qx, qy, qz, &
                            fp1xcor, fp1ycor, fp1zcor, &
                            fp2xcor, fp2ycor, fp2zcor)
        
                fp1x = fp1x + fp1xcor
                fp1y = fp1y + fp1ycor
                fp1z = fp1z + fp1zcor
                fp2x = fp2x + fp2xcor
                fp2y = fp2y + fp2ycor
                fp2z = fp2z + fp2zcor
            end if
             
        !/*
        ! *          If we flipped points 1 and 2 earlier, we have to compensate
        ! *          again here, but all that really needs to be switched are the
        ! *          forces.
        ! */
            if (flip .EQV. .TRUE.) then
                tempx = fp2x
                tempy = fp2y
                tempz = fp2z
                fp2x = fp1x
                fp2y = fp1y
                fp2z = fp1z
                fp1x = tempx
                fp1y = tempy
                fp1z = tempz
            end if
        end if !/* if segment p1->p2 is local */
        
        end subroutine SpecialSegSegForce


        !/*-------------------------------------------------------------------------
        ! *
        ! *      Function:       SegSegForce
        ! *      Description:    Used to calculate the interaction forces between
        ! *                      dislocation segments analytically.
        ! *
        ! *      Arguments:
        ! *              p1*,p2*      endpoints for first dislocation segment starting
        ! *                           at p1x,p1y,p1z and ending at p2x,p2y,p2z
        ! *              p3*,p4*      endpoints for seond dislocation segment starting
        ! *                           at p3x,p3y,p3z and ending at p4x,p4y,p4z
        ! *              bxp,byp,bzp  burgers vector for segment p1 to p2
        ! *              bx,by,bz     burgers vector for segment p3 to p4
        ! *              a            core parameter
        ! *              MU           shear modulus
        ! *              NU           poisson ratio
        ! *              seg12Local   1 if either node of segment p1->p2 is local to
        ! *                           the current domain, zero otherwise.
        ! *              seg34Local   1 if either node of segment p3->p4 is local to
        ! *                           the current domain, zero otherwise.
        ! *              fp1*,fp2*,   pointers to locations in which to return
        ! *              fp3*,fp4*    forces on nodes located at p1, p2, p3 and
        ! *                           p4 respectively
        ! *                      
        ! *-----------------------------------------------------------------------*/
        subroutine SegSegForce(p1x, p1y, p1z, &
                                p2x, p2y, p2z, &
                                p3x, p3y, p3z, &
                                p4x, p4y, p4z, &
                                bpx, bpy, bpz, &
                                bx, by, bz, &
                                seg12Local, seg34Local, &
                                fp1x, fp1y, fp1z, &
                                fp2x, fp2y, fp2z, &
                                fp3x, fp3y, fp3z, &
                                fp4x, fp4y, fp4z)
        use param
        use utils
        implicit none
        real(kind=8)    ::      p1x, p1y, p1z, &
                                p2x, p2y, p2z, &
                                p3x, p3y, p3z, &
                                p4x, p4y, p4z, &
                                bpx, bpy, bpz, &
                                bx, by, bz, &
                                fp1x, fp1y, fp1z, &
                                fp2x, fp2y, fp2z, &
                                fp3x, fp3y, fp3z, &
                                fp4x, fp4y, fp4z
        logical         ::      seg12Local, seg34Local
        intent(inout)   ::      p1x, p1y, p1z, &
                                p2x, p2y, p2z, &
                                p3x, p3y, p3z, &
                                p4x, p4y, p4z, &
                                bpx, bpy, bpz, &
                                bx, by, bz
        intent(in)      ::      seg12Local, seg34Local
        intent(out)     ::      fp1x, fp1y, fp1z, &
                                fp2x, fp2y, fp2z, &
                                fp3x, fp3y, fp3z, &
                                fp4x, fp4y, fp4z
        real(kind=8)    ::      eps, d, c, c2, onemc2, onemc2inv, oneoverL, oneoverLp, &
                                temp1, temp2, temp1a, temp1b, temp2a, temp2b, &
                                R1x, R1y, R1z, R2x, R2y, R2z, &
                                a2, m4p, m4pd, m8p, m8pd, m4pn, m4pnd, m4pnd2, m4pnd3, &
                                a2m4pnd, a2m8pd, a2m4pn, a2m8p, &
                                vec1x, vec1y, vec1z, &
                                tx, ty, tz, &
                                tpx, tpy, tpz, &
                                tctpx, tctpy, tctpz, &
                                ya, yb, za, zb, &
                                f_003a, f_103a, f_013a, f_113a, f_203a, f_023a, f_005a, f_105a, &
                                f_003,  f_103,  f_013,  f_113,  f_203,  f_023,  f_005,  f_105, &
                                f_015a, f_115a, f_205a, f_025a, f_215a, f_125a, f_225a, f_305a, &
                                f_015,  f_115,  f_205,  f_025,  f_215,  f_125,  f_225,  f_305, &
                                f_035a, f_315a, f_135a, &
                                f_035,  f_315,  f_135, &
                                Fint_003, Fint_005, Fint_013, Fint_015, Fint_025, Fint_103, &
                                Fint_105, Fint_115, Fint_125, Fint_205, Fint_215, &
                                I_003x, I_003y, I_003z, I_005x, I_005y, I_005z, &
                                I_013x, I_013y, I_013z, I_015x, I_015y, I_015z, &
                                I_025x, I_025y, I_025z, I_103x, I_103y, I_103z, &
                                I_105x, I_105y, I_105z, I_115x, I_115y, I_115z, &
                                I_125x, I_125y, I_125z, I_205x, I_205y, I_205z, &
                                I_215x, I_215y, I_215z, &
                                I00ax, I00ay, I00az, I01ax, I01ay, I01az, &
                                I10ax, I10ay, I10az, I00bx, I00by, I00bz, &
                                I01bx, I01by, I01bz, I10bx, I10by, I10bz, &
                                bctctpx, bctctpy, bctctpz, &
                                bctdbp, &
                                bctx, bcty, bctz, &
                                bpctpctx, bpctpcty, bpctpctz, &
                                bpctpdb, &
                                bpctpx, bpctpy, bpctpz, &
                                tcbpctx, tcbpcty, tcbpctz, &
                                tcbpdb, &
                                tcbpdtp, &
                                tcbpx, tcbpy, tcbpz, &
                                tctpcbpctx, tctpcbpcty, tctpcbpctz, &
                                tctpcbpdb, &
                                tctpcbpdtp, &
                                tctpcbpx, tctpcbpy, tctpcbpz, &
                                tctpctx, tctpcty, tctpctz, &
                                tctpdb, &
                                tdb, tdbp, &
                                tpcbctpx, tpcbctpy, tpcbctpz, &
                                tpcbdbp, &
                                tpcbdt, &
                                tpcbx, tpcby, tpcbz, &
                                tpctcbctpx, tpctcbctpy, tpctcbctpz, &
                                tpctcbdbp, &
                                tpctcbdt, &
                                tpctcbx, tpctcby, tpctcbz, &
                                tpctctpx, tpctctpy, tpctctpz, &
                                tpctdbp, &
                                tpctx, tpcty, tpctz, &
                                tpdb, tpdbp

        eps = 1.d-6            

        fp1x = 0.d0
        fp1y = 0.d0
        fp1z = 0.d0

        fp2x = 0.d0
        fp2y = 0.d0
        fp2z = 0.d0

        fp3x = 0.d0
        fp3y = 0.d0
        fp3z = 0.d0

        fp4x = 0.d0
        fp4y = 0.d0
        fp4z = 0.d0

        vec1x = p4x - p3x
        vec1y = p4y - p3y
        vec1z = p4z - p3z

        oneoverL = 1./sqrt(vec1x*vec1x+vec1y*vec1y+vec1z*vec1z)

        tx = vec1x*oneoverL
        ty = vec1y*oneoverL
        tz = vec1z*oneoverL

        vec1x = p2x - p1x
        vec1y = p2y - p1y
        vec1z = p2z - p1z

        oneoverLp = 1./sqrt(vec1x*vec1x+vec1y*vec1y+vec1z*vec1z)

        tpx = vec1x*oneoverLp
        tpy = vec1y*oneoverLp
        tpz = vec1z*oneoverLp

        tctpx = ty*tpz - tz*tpy
        tctpy = tz*tpx - tx*tpz
        tctpz = tx*tpy - ty*tpx

        c = tx*tpx + ty*tpy + tz*tpz
        c2 = c*c
        onemc2 = 1. - c2
 
        if (onemc2 .GT. eps) then

            onemc2inv = 1./onemc2

            R1x = p3x - p1x
            R1y = p3y - p1y
            R1z = p3z - p1z

            R2x = p4x - p2x
            R2y = p4y - p2y
            R2z = p4z - p2z

            d = (R2x*tctpx + R2y*tctpy + R2z*tctpz) * onemc2inv

            temp1a = R1x*tx + R1y*ty + R1z*tz
            temp1b = R2x*tx + R2y*ty + R2z*tz

            temp2a = R1x*tpx + R1y*tpy + R1z*tpz
            temp2b = R2x*tpx + R2y*tpy + R2z*tpz

            ya = (temp1a - c*temp2a) * onemc2inv
            yb = (temp1b - c*temp2b) * onemc2inv

            za = (temp2a - c*temp1a) * onemc2inv
            zb = (temp2b - c*temp1b) * onemc2inv


!/*
! *          For this first call to SegSegForceIntegrals() we can
! *          just pass the addresses of f_nnn variables rather than use
! *          the f_nnna variables and then copy the values.
! */
            call SegSegForceIntegrals(d, c, ya, za, f_003, f_103, &
                                    f_013, f_113, f_203, f_023, &
                                    f_005, f_105, f_015, f_115, &
                                    f_205, f_025, f_215, f_125, &
                                    f_225, f_305, f_035, f_315, &
                                    f_135)

            call SegSegForceIntegrals(d, c, ya, zb, f_003a, f_103a, &
                                    f_013a, f_113a, f_203a, f_023a, &
                                    f_005a, f_105a, f_015a, f_115a, &
                                    f_205a, f_025a, f_215a, f_125a, &
                                    f_225a, f_305a, f_035a, f_315a, &
                                    f_135a)

            f_003 = f_003 - f_003a
            f_103 = f_103 - f_103a
            f_013 = f_013 - f_013a
            f_113 = f_113 - f_113a
            f_203 = f_203 - f_203a
            f_023 = f_023 - f_023a
            f_005 = f_005 - f_005a
            f_105 = f_105 - f_105a
            f_015 = f_015 - f_015a
            f_115 = f_115 - f_115a
            f_205 = f_205 - f_205a
            f_025 = f_025 - f_025a
            f_215 = f_215 - f_215a
            f_125 = f_125 - f_125a
            f_225 = f_225 - f_225a
            f_305 = f_305 - f_305a
            f_035 = f_035 - f_035a
            f_315 = f_315 - f_315a
            f_135 = f_135 - f_135a        

            call SegSegForceIntegrals(d, c, yb, za, f_003a, f_103a, &
                                    f_013a, f_113a, f_203a, f_023a, &
                                    f_005a, f_105a, f_015a, f_115a, &
                                    f_205a, f_025a, f_215a, f_125a, &
                                    f_225a, f_305a, f_035a, f_315a, &
                                    f_135a)

            f_003 = f_003 - f_003a
            f_103 = f_103 - f_103a
            f_013 = f_013 - f_013a
            f_113 = f_113 - f_113a
            f_203 = f_203 - f_203a
            f_023 = f_023 - f_023a
            f_005 = f_005 - f_005a
            f_105 = f_105 - f_105a
            f_015 = f_015 - f_015a
            f_115 = f_115 - f_115a
            f_205 = f_205 - f_205a
            f_025 = f_025 - f_025a
            f_215 = f_215 - f_215a
            f_125 = f_125 - f_125a
            f_225 = f_225 - f_225a
            f_305 = f_305 - f_305a
            f_035 = f_035 - f_035a
            f_315 = f_315 - f_315a
            f_135 = f_135 - f_135a

            call SegSegForceIntegrals(d, c, yb, zb, f_003a, f_103a, &
                                        f_013a, f_113a, f_203a, f_023a, &
                                        f_005a, f_105a, f_015a, f_115a, &
                                        f_205a, f_025a, f_215a, f_125a, &
                                        f_225a, f_305a, f_035a, f_315a, &
                                        f_135a)

            f_003 = f_003 + f_003a
            f_103 = f_103 + f_103a
            f_013 = f_013 + f_013a
            f_113 = f_113 + f_113a
            f_203 = f_203 + f_203a
            f_023 = f_023 + f_023a
            f_005 = f_005 + f_005a
            f_105 = f_105 + f_105a
            f_015 = f_015 + f_015a
            f_115 = f_115 + f_115a
            f_205 = f_205 + f_205a
            f_025 = f_025 + f_025a
            f_215 = f_215 + f_215a
            f_125 = f_125 + f_125a
            f_225 = f_225 + f_225a
            f_305 = f_305 + f_305a
            f_035 = f_035 + f_035a
            f_315 = f_315 + f_315a
            f_135 = f_135 + f_135a

            a2 = a*a
            m4p = 0.25d0 * MU / pivalue
            m4pd =  m4p * d
            m8p = 0.5d0 * m4p
            m8pd = m8p * d
            m4pn = m4p / ( 1 - NU )
            m4pnd = m4pn * d
            m4pnd2 = m4pnd * d
            m4pnd3 = m4pnd2 * d
            a2m4pnd = a2 * m4pnd
            a2m8pd = a2 * m8pd
            a2m4pn = a2 * m4pn
            a2m8p = a2 * m8p

            tpctx = -tctpx
            tpcty = -tctpy
            tpctz = -tctpz

            tcbpx = ty*bpz - tz*bpy
            tcbpy = tz*bpx - tx*bpz
            tcbpz = tx*bpy - ty*bpx

            tpcbx = tpy*bz - tpz*by
            tpcby = tpz*bx - tpx*bz
            tpcbz = tpx*by - tpy*bx

            bctx = by*tz - bz*ty
            bcty = bz*tx - bx*tz
            bctz = bx*ty - by*tx


            bpctpx = bpy*tpz - bpz*tpy
            bpctpy = bpz*tpx - bpx*tpz
            bpctpz = bpx*tpy - bpy*tpx

            tdb = tx*bx + ty*by + tz*bz
            tdbp = tx*bpx + ty*bpy + tz*bpz
            tpdb = tpx*bx + tpy*by + tpz*bz
            tpdbp = tpx*bpx + tpy*bpy + tpz*bpz

            tctpdb =  tctpx*bx + tctpy*by + tctpz*bz
            tpctdbp = tpctx*bpx + tpcty*bpy + tpctz*bpz
            tcbpdtp = tpctdbp 
            tpcbdt = tctpdb

            bpctpdb = bpctpx*bx + bpctpy*by + bpctpz*bz
            bctdbp = bctx*bpx + bcty*bpy + bctz*bpz
            tcbpdb = bctdbp
            tpcbdbp = bpctpdb

            tctpctx = tpx - c*tx
            tctpcty = tpy - c*ty
            tctpctz = tpz - c*tz


            tpctctpx = tx - c*tpx
            tpctctpy = ty - c*tpy
            tpctctpz = tz - c*tpz

            tctpcbpx = tdbp*tpx - tpdbp*tx
            tctpcbpy = tdbp*tpy - tpdbp*ty
            tctpcbpz = tdbp*tpz - tpdbp*tz

            tpctcbx = tpdb*tx - tdb*tpx
            tpctcby = tpdb*ty - tdb*tpy
            tpctcbz = tpdb*tz - tdb*tpz

            tcbpctx = bpx - tdbp*tx
            tcbpcty = bpy - tdbp*ty
            tcbpctz = bpz - tdbp*tz

            tpcbctpx = bx - tpdb*tpx
            tpcbctpy = by - tpdb*tpy
            tpcbctpz = bz - tpdb*tpz
   
            bpctpctx = tdbp*tpx - c*bpx
            bpctpcty = tdbp*tpy - c*bpy
            bpctpctz = tdbp*tpz - c*bpz

            bctctpx = tpdb*tx - c*bx
            bctctpy = tpdb*ty - c*by
            bctctpz = tpdb*tz - c*bz

            tctpcbpctx = tdbp*tpctx
            tctpcbpcty = tdbp*tpcty
            tctpcbpctz = tdbp*tpctz

            tpctcbctpx = tpdb*tctpx
            tpctcbctpy = tpdb*tctpy
            tpctcbctpz = tpdb*tctpz

            tctpcbpdtp = tdbp - tpdbp*c
            tpctcbdt = tpdb - tdb*c
            tctpcbpdb =  tdbp*tpdb - tpdbp*tdb
            tpctcbdbp = tctpcbpdb

!/*
! *          Only calculate the forces for segment p3->p4 if at least one
! *          of the segment's nodes is local to the current domain.
! */
            if (seg34Local) then

                temp1 = tdbp*tpdb + tctpcbpdb

                I00ax =  temp1 * tpctx
                I00ay =  temp1 * tpcty
                I00az =  temp1 * tpctz

                I00bx =  bctx * tctpcbpdtp
                I00by =  bcty * tctpcbpdtp
                I00bz =  bctz * tctpcbpdtp

                temp1 = (m4pnd * tctpdb)
                temp2 = (m4pnd * bpctpdb)

                I_003x = m4pd*I00ax - m4pnd*I00bx + temp1*bpctpctx + &
                            temp2*tctpctx 
                I_003y = m4pd*I00ay - m4pnd*I00by + temp1*bpctpcty + &
                            temp2*tctpcty 
                I_003z = m4pd*I00az - m4pnd*I00bz + temp1*bpctpctz + &
                            temp2*tctpctz 

                temp1 = (m4pnd3 * tctpcbpdtp*tctpdb)
                
                I_005x = a2m8pd*I00ax - a2m4pnd*I00bx - temp1*tctpctx
                I_005y = a2m8pd*I00ay - a2m4pnd*I00by - temp1*tctpcty
                I_005z = a2m8pd*I00az - a2m4pnd*I00bz - temp1*tctpctz


                I10ax = tcbpctx*tpdb - tctpx*tcbpdb
                I10ay = tcbpcty*tpdb - tctpy*tcbpdb
                I10az = tcbpctz*tpdb - tctpz*tcbpdb

                I10bx = bctx * tcbpdtp
                I10by = bcty * tcbpdtp
                I10bz = bctz * tcbpdtp

                temp1 = (m4pn * tdb)

                I_103x = temp1*bpctpctx + m4p*I10ax - m4pn*I10bx
                I_103y = temp1*bpctpcty + m4p*I10ay - m4pn*I10by
                I_103z = temp1*bpctpctz + m4p*I10az - m4pn*I10bz

                temp1 = m4pnd2 * (tcbpdtp*tctpdb + tctpcbpdtp*tdb)

                I_105x = a2m8p*I10ax - a2m4pn*I10bx - temp1*tctpctx
                I_105y = a2m8p*I10ay - a2m4pn*I10by - temp1*tctpcty
                I_105z = a2m8p*I10az - a2m4pn*I10bz - temp1*tctpctz

                I01ax = tctpx*bpctpdb - bpctpctx*tpdb
                I01ay = tctpy*bpctpdb - bpctpcty*tpdb
                I01az = tctpz*bpctpdb - bpctpctz*tpdb

                temp1 = (m4pn * tpdb) 
                temp2 = (m4pn * bpctpdb)

                I_013x = m4p*I01ax + temp1*bpctpctx - temp2*tctpx
                I_013y = m4p*I01ay + temp1*bpctpcty - temp2*tctpy
                I_013z = m4p*I01az + temp1*bpctpctz - temp2*tctpz

                temp1 = (m4pnd2 * tctpcbpdtp * tpdb)
                temp2 = (m4pnd2 * tctpcbpdtp * tctpdb)

                I_015x = a2m8p*I01ax - temp1*tctpctx + temp2*tctpx
                I_015y = a2m8p*I01ay - temp1*tctpcty + temp2*tctpy
                I_015z = a2m8p*I01az - temp1*tctpctz + temp2*tctpz

                temp1 = (m4pnd * tcbpdtp * tdb)

                I_205x = -temp1 * tctpctx
                I_205y = -temp1 * tctpcty
                I_205z = -temp1 * tctpctz

                temp1 = (m4pnd * tctpcbpdtp * tpdb) 

                I_025x = temp1 * tctpx 
                I_025y = temp1 * tctpy
                I_025z = temp1 * tctpz

                temp1 = (m4pnd * (tctpcbpdtp*tdb + tcbpdtp*tctpdb))
                temp2 = (m4pnd * tcbpdtp * tpdb)

                I_115x =  temp1*tctpx - temp2*tctpctx
                I_115y =  temp1*tctpy - temp2*tctpcty
                I_115z =  temp1*tctpz - temp2*tctpctz

                temp1 = (m4pn * tcbpdtp * tdb)

                I_215x = temp1 * tctpx
                I_215y = temp1 * tctpy
                I_215z = temp1 * tctpz

                temp1 = (m4pn * tcbpdtp * tpdb)


                I_125x = temp1 * tctpx
                I_125y = temp1 * tctpy
                I_125z = temp1 * tctpz
     
  
                Fint_003 = f_103 - ya*f_003
                Fint_103 = f_203 - ya*f_103
                Fint_013 = f_113 - ya*f_013
                Fint_005 = f_105 - ya*f_005
                Fint_105 = f_205 - ya*f_105
                Fint_015 = f_115 - ya*f_015
                Fint_115 = f_215 - ya*f_115
                Fint_205 = f_305 - ya*f_205
                Fint_025 = f_125 - ya*f_025
                Fint_215 = f_315 - ya*f_215
                Fint_125 = f_225 - ya*f_125

                fp4x = (I_003x*Fint_003 + I_103x*Fint_103 + I_013x*Fint_013 + &
                            I_005x*Fint_005 + I_105x*Fint_105 + I_015x*Fint_015 + &
                            I_115x*Fint_115 + I_205x*Fint_205 + I_025x*Fint_025 + &
                            I_215x*Fint_215 + I_125x*Fint_125) * oneoverL

                fp4y = (I_003y*Fint_003 + I_103y*Fint_103 + I_013y*Fint_013 + &
                            I_005y*Fint_005 + I_105y*Fint_105 + I_015y*Fint_015 + &
                            I_115y*Fint_115 + I_205y*Fint_205 + I_025y*Fint_025 + &
                            I_215y*Fint_215 + I_125y*Fint_125) * oneoverL

                fp4z = (I_003z*Fint_003 + I_103z*Fint_103 + I_013z*Fint_013 + &
                            I_005z*Fint_005 + I_105z*Fint_105 + I_015z*Fint_015 + &
                            I_115z*Fint_115 + I_205z*Fint_205 + I_025z*Fint_025 + &
                            I_215z*Fint_215 + I_125z*Fint_125) * oneoverL

                Fint_003 = yb*f_003 - f_103
                Fint_103 = yb*f_103 - f_203
                Fint_013 = yb*f_013 - f_113
                Fint_005 = yb*f_005 - f_105
                Fint_105 = yb*f_105 - f_205
                Fint_015 = yb*f_015 - f_115
                Fint_115 = yb*f_115 - f_215
                Fint_205 = yb*f_205 - f_305
                Fint_025 = yb*f_025 - f_125
                Fint_215 = yb*f_215 - f_315
                Fint_125 = yb*f_125 - f_225
  
                fp3x = (I_003x*Fint_003 + I_103x*Fint_103 + I_013x*Fint_013 + &
                            I_005x*Fint_005 + I_105x*Fint_105 + I_015x*Fint_015 + & 
                            I_115x*Fint_115 + I_205x*Fint_205 + I_025x*Fint_025 + &
                            I_215x*Fint_215 + I_125x*Fint_125) * oneoverL

                fp3y = (I_003y*Fint_003 + I_103y*Fint_103 + I_013y*Fint_013 + &
                            I_005y*Fint_005 + I_105y*Fint_105 + I_015y*Fint_015 + &
                            I_115y*Fint_115 + I_205y*Fint_205 + I_025y*Fint_025 + &
                            I_215y*Fint_215 + I_125y*Fint_125) * oneoverL

                fp3z = (I_003z*Fint_003 + I_103z*Fint_103 + I_013z*Fint_013 + &
                            I_005z*Fint_005 + I_105z*Fint_105 + I_015z*Fint_015 + &
                            I_115z*Fint_115 + I_205z*Fint_205 + I_025z*Fint_025 + &
                            I_215z*Fint_215 + I_125z*Fint_125) * oneoverL

            end if ! if segment p3->p4 is "local"
                
!/*
! *          Only calculate the forces for segment p1->p2 if at least one
! *          of the segment's nodes is local to the current domain.
! */
            if (seg12Local) then

                temp1 = tpdb*tdbp + tpctcbdbp

                I00ax = temp1 * tctpx
                I00ay = temp1 * tctpy
                I00az = temp1 * tctpz

                I00bx = bpctpx * tpctcbdt
                I00by = bpctpy * tpctcbdt
                I00bz = bpctpz * tpctcbdt

                temp1 = m4pnd * tpctdbp
                temp2 = m4pnd * bctdbp

                I_003x = m4pd*I00ax - m4pnd*I00bx + temp1*bctctpx + &
                            temp2*tpctctpx
                I_003y = m4pd*I00ay - m4pnd*I00by + temp1*bctctpy + &
                            temp2*tpctctpy
                I_003z = m4pd*I00az - m4pnd*I00bz + temp1*bctctpz + &
                            temp2*tpctctpz

                temp1 = m4pnd3 * tpctcbdt * tpctdbp

                I_005x = a2m8pd*I00ax - a2m4pnd*I00bx - temp1*tpctctpx 
                I_005y = a2m8pd*I00ay - a2m4pnd*I00by - temp1*tpctctpy 
                I_005z = a2m8pd*I00az - a2m4pnd*I00bz - temp1*tpctctpz 

                I01ax = tpctx*tpcbdbp - tpcbctpx*tdbp
                I01ay = tpcty*tpcbdbp - tpcbctpy*tdbp
                I01az = tpctz*tpcbdbp - tpcbctpz*tdbp

                I01bx = -bpctpx * tpcbdt
                I01by = -bpctpy * tpcbdt
                I01bz = -bpctpz * tpcbdt

                temp1 = m4pn * tpdbp

                I_013x = -temp1 * bctctpx + m4p*I01ax - m4pn*I01bx
                I_013y = -temp1 * bctctpy + m4p*I01ay - m4pn*I01by
                I_013z = -temp1 * bctctpz + m4p*I01az - m4pn*I01bz

                temp1 = m4pnd2 * (tpcbdt*tpctdbp + tpctcbdt*tpdbp)

                I_015x = a2m8p*I01ax - a2m4pn*I01bx + temp1*tpctctpx
                I_015y = a2m8p*I01ay - a2m4pn*I01by + temp1*tpctctpy
                I_015z = a2m8p*I01az - a2m4pn*I01bz + temp1*tpctctpz

                I10ax = bctctpx*tdbp - tpctx*bctdbp
                I10ay = bctctpy*tdbp - tpcty*bctdbp
                I10az = bctctpz*tdbp - tpctz*bctdbp

                temp1 = m4pn * tdbp 
                temp2 = m4pn * bctdbp

                I_103x = m4p*I10ax - temp1*bctctpx + temp2*tpctx
                I_103y = m4p*I10ay - temp1*bctctpy + temp2*tpcty
                I_103z = m4p*I10az - temp1*bctctpz + temp2*tpctz

                temp1 = m4pnd2 * tpctcbdt * tdbp
                temp2 = m4pnd2 * tpctcbdt * tpctdbp

                I_105x = a2m8p*I10ax + temp1*tpctctpx - temp2*tpctx
                I_105y = a2m8p*I10ay + temp1*tpctctpy - temp2*tpcty
                I_105z = a2m8p*I10az + temp1*tpctctpz - temp2*tpctz

                temp1 = (m4pnd * tpcbdt * tpdbp)

                I_025x = -temp1 * tpctctpx
                I_025y = -temp1 * tpctctpy
                I_025z = -temp1 * tpctctpz

                temp1 = (m4pnd * tpctcbdt * tdbp)

                I_205x = temp1 * tpctx
                I_205y = temp1 * tpcty
                I_205z = temp1 * tpctz

                temp1 = m4pnd * (tpctcbdt*tpdbp + tpcbdt*tpctdbp)
                temp2 = m4pnd * tpcbdt * tdbp

                I_115x = temp1*tpctx - temp2*tpctctpx
                I_115y = temp1*tpcty - temp2*tpctctpy
                I_115z = temp1*tpctz - temp2*tpctctpz

                temp1 = (m4pn * tpcbdt * tpdbp)

                I_125x = -temp1 * tpctx
                I_125y = -temp1 * tpcty
                I_125z = -temp1 * tpctz

                temp1 = (m4pn * tpcbdt * tdbp)

                I_215x = -temp1 * tpctx
                I_215y = -temp1 * tpcty
                I_215z = -temp1 * tpctz

                Fint_003 = f_013 - zb*f_003
                Fint_103 = f_113 - zb*f_103
                Fint_013 = f_023 - zb*f_013
                Fint_005 = f_015 - zb*f_005
                Fint_105 = f_115 - zb*f_105
                Fint_015 = f_025 - zb*f_015
                Fint_115 = f_125 - zb*f_115
                Fint_205 = f_215 - zb*f_205
                Fint_025 = f_035 - zb*f_025
                Fint_215 = f_225 - zb*f_215
                Fint_125 = f_135 - zb*f_125

                fp1x = (I_003x*Fint_003 + I_103x*Fint_103 + I_013x*Fint_013 + &
                            I_005x*Fint_005 + I_105x*Fint_105 + I_015x*Fint_015 + &
                            I_115x*Fint_115 + I_205x*Fint_205 + I_025x*Fint_025 + &
                            I_215x*Fint_215 + I_125x*Fint_125) * oneoverLp

                fp1y = (I_003y*Fint_003 + I_103y*Fint_103 + I_013y*Fint_013 + &
                            I_005y*Fint_005 + I_105y*Fint_105 + I_015y*Fint_015 + &
                            I_115y*Fint_115 + I_205y*Fint_205 + I_025y*Fint_025 + &
                            I_215y*Fint_215 + I_125y*Fint_125) * oneoverLp

                fp1z = (I_003z*Fint_003 + I_103z*Fint_103 + I_013z*Fint_013 + &
                            I_005z*Fint_005 + I_105z*Fint_105 + I_015z*Fint_015 + &
                            I_115z*Fint_115 + I_205z*Fint_205 + I_025z*Fint_025 + &
                            I_215z*Fint_215 + I_125z*Fint_125) * oneoverLp
   
                Fint_003 = za*f_003 - f_013
                Fint_103 = za*f_103 - f_113
                Fint_013 = za*f_013 - f_023
                Fint_005 = za*f_005 - f_015
                Fint_105 = za*f_105 - f_115
                Fint_015 = za*f_015 - f_025
                Fint_115 = za*f_115 - f_125
                Fint_205 = za*f_205 - f_215
                Fint_025 = za*f_025 - f_035
                Fint_215 = za*f_215 - f_225
                Fint_125 = za*f_125 - f_135

                fp2x = (I_003x*Fint_003 + I_103x*Fint_103 + I_013x*Fint_013 + &
                            I_005x*Fint_005 + I_105x*Fint_105 + I_015x*Fint_015 + &
                            I_115x*Fint_115 + I_205x*Fint_205 + I_025x*Fint_025 + &
                            I_215x*Fint_215 + I_125x*Fint_125) * oneoverLp

                fp2y = (I_003y*Fint_003 + I_103y*Fint_103 + I_013y*Fint_013 + &
                            I_005y*Fint_005 + I_105y*Fint_105 + I_015y*Fint_015 + &
                            I_115y*Fint_115 + I_205y*Fint_205 + I_025y*Fint_025 + &
                            I_215y*Fint_215 + I_125y*Fint_125) * oneoverLp

                fp2z = (I_003z*Fint_003 + I_103z*Fint_103 + I_013z*Fint_013 + &
                            I_005z*Fint_005 + I_105z*Fint_105 + I_015z*Fint_015 + &
                            I_115z*Fint_115 + I_205z*Fint_205 + I_025z*Fint_025 + &
                            I_215z*Fint_215 + I_125z*Fint_125) * oneoverLp
   
            end if ! if segment p1->p2 is "local"

        else
!/*
! *          The two lines are parallel, so we have to use a special
! *          lower dimensional function
! */
            call SpecialSegSegForce(p1x, p1y, p1z, p2x, p2y, p2z,  &
                                p3x, p3y, p3z, p4x, p4y, p4z, &
                                bpx, bpy, bpz, bx, by, bz,  &
                                eps, seg12Local, seg34Local, &
                                fp1x, fp1y, fp1z, fp2x, fp2y, fp2z, &
                                fp3x, fp3y, fp3z, fp4x, fp4y, fp4z)
        end if

        end subroutine SegSegForce
    ! ###################  

    ! ###################    	
	    subroutine soluteforce_num(rn, links, Psolutes, fnsolutes)
	
        use param
        use utils
	    implicit none
	
        integer, parameter  ::  Nnum = 10
	    real(kind=8)	::  rn(Nntot,3), fnsolutes(Nntot,3), Psolutes(Nstot,3), &
                            rnd(3), rp(3), fPK(3), drincr, nrnd, t(3), stress(3,3)
	    integer 		::  links(Nntot-1,2), i, j, n1, n2
	
	    intent(in)		::  rn, links, Psolutes
	    intent(out)		:: 	fnsolutes
                                
        fnsolutes = 0.d0
        
        ! loop over all segments
	    do i=1, Nnodes-1
		    n1 = links(i,1)
		    n2 = links(i,2)
		    rnd = rn(n2,:) - rn(n1,:)
            nrnd = norm(rnd)
            t = rnd/nrnd
            drincr = nrnd/Nnum
            
            ! calculate PK force at grid points between nodes n1 and n2
            do j=0, Nnum
                rp = rn(n1,:) + rnd*j/Nnum
                call solutestress(Psolutes, rp, stress)
                fPK = cross(mdot(stress, bv), t)
                ! integrate for both end points of the segment n1, n2
                fnsolutes(n1,:) = fnsolutes(n1,:) + fPK*(Nnum-j)/Nnum *drincr
                fnsolutes(n2,:) = fnsolutes(n2,:) + fPK*j/Nnum *drincr
            end do
        end do
               
      end subroutine soluteforce_num
      ! ###################

      
      ! ###################    	
      subroutine solutestress1(Psolutes, pos, stress, ir, grid_pos, Ps_grid, nPS_grid)
        
        use param
        use utils
        implicit none
        
        integer			::  i, kk,l,m,ir,&
             grid_pos(nntot,2), Ps_grid(nngridtot,nngridtot,Nsgridloc), nPS_grid(nngridtot,nngridtot)

        real(kind=8)	::  Psolutes(Nstot,3), stress(3,3), s(3,3), x, y, z, xS, yS, zS, &
             G, R, K, delta, fact1, fact2, tempR, x_xS, y_yS, z_zS, &
             x_xS2, y_yS2, z_zS2, pos(3)
        
        intent(in)		::  Psolutes, pos
        intent(out)		::  stress
        
        x = pos(1); y = pos(2); z = pos(3)
        G = solGRKdRvec(1); R = solGRKdRvec(2); K = solGRKdRvec(3); delta = solGRKdRvec(4)
        fact1 = (2.d0*G*delta*R**2)/(1.d0+(4.d0*G)/(3.d0*K))        
        stress = 0.d0
        
        ! loop over all nearby solutes
        kk = grid_pos(ir,1)
        l = grid_pos(ir,2)
        do m = 1, nPS_grid(kk,l)
           i = Ps_grid(kk,l,m)
           xS = Psolutes(i,1)
           yS = Psolutes(i,2)
           zS = Psolutes(i,3)
           
           x_xS  = x-xS
           y_yS  = y-yS
           z_zS  = z-zS
           x_xS2 = x_xS**2
           y_yS2 = y_yS**2
           z_zS2 = z_zS**2
            
           tempR = sqrt(x_xS2 + y_yS2 + z_zS2)
           fact2 = fact1/(tempR**5) 
           
           s(1,1) = (z_zS2 + y_yS2 - 2.d0*x_xS2) 
           s(2,2) = (z_zS2 + x_xS2 - 2.d0*y_yS2)
           s(3,3) = (x_xS2 + y_yS2 - 2.d0*z_zS2)
           s(1,2) = (-3.d0*x_xS*y_yS)
           s(1,3) = (-3.d0*x_xS*z_zS)
           s(2,3) = (-3.d0*y_yS*z_zS)
           s(2,1) = s(1,2)
           s(3,1) = s(1,3)
           s(3,2) = s(2,3)           
           s = s * fact2
           
           stress = stress + s
        end do
            
      end subroutine solutestress1
      ! ###################
      ! ###################    	
      subroutine solutestress(Psolutes, pos, stress)
        
        use param
        use utils
        implicit none
        
        integer			::  i
        real(kind=8)	::  Psolutes(Nstot,3), stress(3,3), s(3,3), x, y, z, xS, yS, zS, &
             G, R, K, delta, fact1, fact2, tempR, x_xS, y_yS, z_zS, &
             x_xS2, y_yS2, z_zS2, pos(3)
        
        intent(in)		::  Psolutes, pos
        intent(out)		:: 	stress
        
        x = pos(1); y = pos(2); z = pos(3)
        G = solGRKdRvec(1); R = solGRKdRvec(2); K = solGRKdRvec(3); delta = solGRKdRvec(4)
        fact1 = (2.d0*G*delta*R**2)/(1.d0+(4.d0*G)/(3.d0*K))        
        stress = 0.d0
        
        ! loop over all solutes
        do i=1, Nsolutes
           xS = Psolutes(i,1)
           yS = Psolutes(i,2)
           zS = Psolutes(i,3)
           
           x_xS  = x-xS
           y_yS  = y-yS
           z_zS  = z-zS
           x_xS2 = x_xS**2
           y_yS2 = y_yS**2
           z_zS2 = z_zS**2
            
           tempR = sqrt(x_xS2 + y_yS2 + z_zS2)
           fact2 = fact1/(tempR**5) 
           
           s(1,1) = (z_zS2 + y_yS2 - 2.d0*x_xS2) 
           s(2,2) = (z_zS2 + x_xS2 - 2.d0*y_yS2)
           s(3,3) = (x_xS2 + y_yS2 - 2.d0*z_zS2)
           s(1,2) = (-3.d0*x_xS*y_yS)
           s(1,3) = (-3.d0*x_xS*z_zS)
           s(2,3) = (-3.d0*y_yS*z_zS)
           s(2,1) = s(1,2)
           s(3,1) = s(1,3)
           s(3,2) = s(2,3)           
           s = s * fact2
           
           stress = stress + s
        end do
            
      end subroutine solutestress
      ! ###################
        
      
      
      
      
      
      ! ###################
      ! Find neighbour dislocation segments
      subroutine ele_ngbrs(rn, links3, nb_neigbs, ele_neigbs)
        
        use param
        use utils
        implicit none
        
        real(kind=8)	::  rn(Nntot,3), pB(3), pC(3),  proj_pB, proj_pC
        integer         ::  links3(Nntot,3), disl(Ndtot,2), inode, &
             ele,  nb_neigbs(Nntot), ele_neigbs(Nele_ngbs,Nele_ngbs)
        
        intent(in)      ::  rn, links3
        ! loop over all nodes
        do inode = 1, Nnodes
           nb_neigbs(inode)=0
           ! loop over all the elements
           do ele = 1, Nnodes
              pB = rn(links3(ele,2),:)
              pC = rn(links3(ele,3),:)
              if ((Bcond .EQ. 'PER') .AND. (norm(pB-pC) .GT. 0.5d0*boxsize)) then     ! check if we deal with the node on the other side of periodic box, if so, reflect it
                 call reflect(pC)
              end if
              if ( isfixed(links3(ele,2)) .AND. isfixed(links3(ele,3)) ) then    ! if element is between two fixed nodes, this gives no contribution to stress
                 cycle
              end if
              ! skip elements: ele=Nnodes/2 and ele=Nnodes, e.g. from Nnodes/2 to Nnodes/2 +1
              !if (sqrt(vdot(pB-pC,pB-pC)).ge.0.5d0*boxsize) cycle
              ! check if pB or pC is close to regarded node
              if ((norm(pB-rn(inode,:)).lt.rmaxstress).or.(norm(pC-rn(inode,:)).lt.rmaxstress)) then
                 nb_neigbs(inode) = nb_neigbs(inode) + 1
                 ele_neigbs(inode,nb_neigbs(inode)) = ele
              else ! the element should only be included once here, hence "else"
                 If(Bcond .eq. 'PER') then
                    ! if the element is within the REFLECT_RATIO*BOXSIZE distance from the periodic boundary
                    ! then reflect it and add its contribution to the stress
                    proj_pB = vdot(pB,pervec)
                    proj_pC = vdot(pC,pervec)
                    if ((0.5d0-abs(0.5d0*(proj_pB+proj_pc))/boxsize) .LT. reflect_ratio) then
                       pB = pB - sign(boxsize,proj_pB)*pervec
                       pC = pC  - sign(boxsize,proj_pC)*pervec
                       if ((norm(pB-rn(inode,:)).lt.rmaxstress).or.(norm(pC-rn(inode,:)).lt.rmaxstress)) then
                          nb_neigbs(inode) = nb_neigbs(inode) + 1
                          ele_neigbs(inode,nb_neigbs(inode)) = ele
                       endif
                    endif
                 endif
              endif
           end do
        enddo
        
      end subroutine ele_ngbrs
      ! ###################
      
      
    end module dynamics



