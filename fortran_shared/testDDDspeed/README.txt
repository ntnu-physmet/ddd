A dislocation gliding at a prescribed speed

Compile the fortran files in ../. by using the gfortran compiler. The compilation is prescribed by the Makefile. 
Copy the compiled executable file "DDDspeed.exe" from ../.
Edit "input.dat" to prescribe the size of the system, the number of solutes, the misfit, etc
Run "DDDspeed.exe".

Output:

gnuscript: a unix script that produces disl.gif, which is a 3D animation of the dislocation glide

gnuscript2: a unix script that produces disl2.gif, which is an animated gif. Note that gifsicle is run to optimize the animated gif at the end of this script.

points.dat: location of the solute atoms
disl_line.dat: dislocation line at successive positions during glide
xpos.dat: tau as a function of xmean
