    program DDDplt
    
    use param
    use utils
    use initialize
    use dynamics
    use topology
    use printing

    implicit none
    !Nsolutes = Area/L_s^2
    integer, parameter ::  id_file1 = 15, id_file2 = 16, id_file3 = 17, id_file4= 18, id_file5=19

    real(kind=8)    ::  rnew(Nntot,3), rn(Nntot,3), vn(Nntot,3), Psolutes(Nstot,3), & 
         dt, appliedstress(3,3), distMat(Nntot, Nntot), cP(3), xave, &
         xave0, xave00, vave, rguess1(Nntot,3), xaveguess, xaveplt
    integer         ::  links(Nntot-1,2), links3(Nntot,3), disl(Ndtot,2), curstep, &
         distMatID(Nntot, Nntot), cnode, cele
    integer         ::  iout, out_dinc, ioutstep,i,j, iplt
    real(kind=8)    ::  tau, Lzmin, Lzmax, Lxmin, Lxmax, xavemax, timetot,rnd1
    logical         ::  remeshed, flag, convergent, iscoarsend, isrefined

    open(id_file1, file="disl_line.dat")
    open(id_file2, file="points.dat")
    open(id_file3, file="gnuscript")
    open(id_file5, file="gnuscript2")
    open(id_file4, file="xpos.dat")

    ! remember rmaxstress = 20b in initialze, change if large splitting

    iplt=1
    iout=1  
    out_dinc = 1
    timetot  = 0.
    call init_params
    ioutstep = 300
    Lzmin=bounds(1)
    Lzmax=bounds(2)
    Lxmin=bounds(3)
    Lxmax=bounds(4)
    xavemax=(Lxmax-Lxmin)-5.
    
    call init_disloc(rn, links, links3, disl)
    call init_solutes(Psolutes)

    rcmin = 20.

    call prt_solutes(Psolutes, id_file2)
    call prt_disl(rn, id_file1)
    
    ! calculate average glide positon of the original line.
    xave0 = 0.d0
    do j = 1,Nnodes
       xave0 =xave0 + vdot(rn(j,:),cross(sv,pervec))
    enddo
    xave0 = xave0/Nnodes
    
    tau = 0.d0
    do i=1,3
       do j=1,3
          appliedstress(i,j) = tau*(sv(j)*bv(i) + sv(i)*bv(j))
       enddo
    enddo

    totalsteps = 0

    ! first do relaxation with tau=0.
    do curstep = 1, totalsteps
       call integrate(rn, links, links3, disl, distMat, distMatID, Psolutes, appliedstress, vn, dt, convergent)
       timetot=timetot+dt
       if (.NOT. convergent) then
          exit
       end if
       ! calculate average glide positon of the original line.
       xave = 0.d0
       do j = 1,Nnodes
          xave=xave + vdot(rn(j,:),cross(sv,pervec))
       enddo
       xave = xave/Nnodes
       vave = (xave-xave0)/dt
       xave0= xave

       if((curstep/iplt)*iplt.eq.curstep) then
          write(*,*) curstep
          write(*,*) "x, v, t : ",xave,vave, timetot
          write(id_file4,*) xave, tau
       endif

       if(vave.lt.1.d-3) then
          write(*,*) "x relaxed: ",xave
          xave0 =xave
          exit
       endif
    end do


    ! main time loop
    xave00  =xave0
    xaveplt =xave0
    tau = 1.D-4
    do i=1,3
       do j=1,3
          appliedstress(i,j) = tau*(sv(j)*bv(i) + sv(i)*bv(j))
       enddo
    enddo

    totalsteps = 10000000
    
    ! loop over all steps
    do curstep = 1, totalsteps
       call integrate(rn, links, links3, disl, distMat, distMatID, Psolutes, appliedstress, vn, dt, convergent)
       timetot=timetot+dt
       if (.NOT. convergent) then
          exit
       end if
       ! calculate average glide positon of the original line.
       xave = 0.d0
       do j = 1,Nnodes
          xave=xave + vdot(rn(j,:),cross(sv,pervec))
       enddo
       xave = xave/Nnodes
       vave = (xave-xave0)/dt
       xave0 = xave

       call redist(rn)

       !if(curstep .eq. (curstep/iplt)*iplt) then
       if(xave-xaveplt.ge.0.1) then
          xaveplt = xave
          iout = iout + 1
          call prt_disl(rn, id_file1)
       endif
       if(curstep .eq. (curstep/ioutstep)*ioutstep) then
          write(*,*) curstep, xave, vave, tau
       endif

       write(id_file4,*) xave, tau

       if(vave.lt.1.d-3) then
          rguess1=rn
          xaveguess=xave
          tau=tau*1.02
          write(*,*) "x,tau,vave: ",xave,tau, vave
       endif
       do i=1,3
          do j=1,3
             appliedstress(i,j) = tau*(sv(j)*bv(i) + sv(i)*bv(j))
          enddo
       enddo
       if((xave-xave00).gt.xavemax) exit
    end do

    
    Lzmin=bounds(1)
    Lzmax=bounds(2)
    Lxmin=bounds(3)
    Lxmax=bounds(4)

    call mk_script(iout, Lzmin, Lzmax, Lxmin, Lxmax, id_file3) 
    
    Lzmin=bounds(1)
    Lzmax=bounds(2)
    Lxmin=bounds(3)
    Lxmax=bounds(4)

    call mk_script2(iout, Lzmin, Lzmax, Lxmin, Lxmax, id_file5) 

    write(*,*) "Finished succesfully!"
    write(*,*) rn(Nnodes/2,:)
    write(*,*) dt0
    read(*,*)
  
    close(id_file1)
    close(id_file2)
    close(id_file3)
    close(id_file4)
    close(id_file5)
    
    end program DDDplt
    
