module param
    
    implicit none
    integer, parameter      ::  dp = selected_real_kind(p=13, r=200),   & 
                                Nntot = 26000,                            &
                                Nstot = 460000,                          &
                                Ndtot = 2,                              &
                                Nsgridloc = 700,                          &
                                Nngridtot = Nntot/15,                   &
                                Nele_ngbs = 100
!                                Nntot = 13000,                            &
!                                Nstot = 60000,                          &
!                                Ndtot = 2,                              &
!                                Nsgridloc = 700,                          &
!                                Nngridtot = Nntot/15,                   &
!                                Nele_ngbs = 100
   
    integer                 ::  Nnodes, Nsolutes, Ndisl, totalsteps
    integer, allocatable    ::  fixed(:), endpt(:)
    real(kind=8)            ::  boxsize, bv(3), sv(3), solGRKdRvec(4), appliedstress0(3,3), &
                                bounds(4), MU, NU, a, Beclimb, Bline, dt0, lmin, lmax, rcmin, &
                                pervec(3), rmaxstress, bvlead(3), bvtrail(3), sfe, offset(3), &
                                reflect_ratio, corefactor
                                
    character(len=3)        ::  method, int_scheme, Bcond
    character(len=8)        ::  analysis, disl_type

    contains
        ! ###################
        subroutine init_params
        
        use utils
        integer                 ::  ierror, i,j
        character(len=150)      ::  filename, tmp
        real(kind=8)            :: b, t, svlength, tau_inn

        !write(*,*) 'Input file: '
        !read(*,*) filename
        open(10, file='input.dat', status='OLD', iostat=ierror)

        if ( ierror .EQ. 0 ) then
            read(10, fmt='(A)') tmp
            read(10, *) Nnodes
            read(10, fmt='(A)') tmp
            read(10, *) Nsolutes
            read(10, fmt='(A)') tmp
            read(10, *) boxsize
            read(10, fmt='(A)') tmp
            read(10, *) reflect_ratio
            read(10, fmt='(A)') tmp
            read(10, *) bv(1), bv(2), bv(3)
            ! normalize the Burgers vector!!!
            b=sqrt(bv(1)**2+bv(2)**2+bv(3)**2)
            bv(:)=bv(:)/b
            read(10, fmt='(A)') tmp
            read(10, *) sv(1), sv(2), sv(3)
            svlength = sqrt(sv(1)**2+sv(2)**2+sv(3)**2)
            sv(:)=sv/svlength

            if(vdot(bv,sv).gt.1.E-6) then
               write(*,*) "illegal choice of b and sv"
            end if
            
            read(10, fmt='(A)') tmp
            !read(10, *) disl_type
            read(10, *) pervec(1),pervec(2),pervec(3)
            t=sqrt(pervec(1)**2+pervec(2)**2+pervec(3)**2)
            pervec(:) = pervec(:)/t
            
            if(vdot(pervec,sv).gt.1.E-6) then
               write(*,*) "illegal choice of pervec (t not in slip plane)"
            end if
            
            read(10, fmt='(A)') tmp
            read(10, *) solGRKdRvec(1), solGRKdRvec(2), solGRKdRvec(3), solGRKdRvec(4)
            read(10, fmt='(A)') tmp
            read(10, *) bounds(1), bounds(2), bounds(3), bounds(4)
            read(10, fmt='(A)') tmp
            read(10, *) MU
            read(10, fmt='(A)') tmp
            read(10, *) NU
            read(10, fmt='(A)') tmp
            read(10, *) Beclimb
            read(10, fmt='(A)') tmp
            read(10, *) Bline
            read(10, fmt='(A)') tmp
            read(10, *) method
            read(10, fmt='(A)') tmp
            read(10, *) int_scheme
            read(10, fmt='(A)') tmp
            read(10, *) Bcond
            read(10, fmt='(A)') tmp
            read(10, *) analysis
            read(10, fmt='(A)') tmp
            read(10, *) sfe
            read(10, fmt='(A)') tmp
            read(10, *) totalsteps
            read(10, fmt='(A)') tmp
            read(10, *) dt0
            read(10, fmt='(A)') tmp
            read(10,*) tau_inn
            bvtrail  = norm(bv)*(bv -cross(bv,sv)/sqrt(3.d0))/norm(bv -cross(bv,sv)/sqrt(3.d0))/sqrt(3.d0)
            bvlead   = norm(bv)*(bv +cross(bv,sv)/sqrt(3.d0))/norm(bv +cross(bv,sv)/sqrt(3.d0))/sqrt(3.d0)

            write(*,*) "Burgers vector"
            write(*,*) bv
            write(*,*)
            write(*,*) "normal vector"
            write(*,*) sv
            write(*,*)
            do i=1,3
               do j=1,3
                  appliedstress0(i,j) = tau_inn*(sv(j)*bv(i) + sv(i)*bv(j))
                  !appliedstress0(i,j) = tau_inn*(sv(j)*bvtrail(i) + sv(i)*bvtrail(j))
                  !appliedstress0(i,j) = tau_inn*(sv(j)*bvlead(i) + sv(i)*bvlead(j))
               enddo
            enddo
            write(*,*) "applied stress:"
            write(*, *) appliedstress0(1,1), appliedstress0(1,2), appliedstress0(1,3)
            write(*, *) appliedstress0(2,1), appliedstress0(2,2), appliedstress0(2,3)
            write(*, *) appliedstress0(3,1), appliedstress0(3,2), appliedstress0(3,3)
            write(*,*)
            !read(10, *) appliedstress0(1,1), appliedstress0(1,2), appliedstress0(1,3)
            !read(10, *) appliedstress0(2,1), appliedstress0(2,2), appliedstress0(2,3)
            !read(10, *) appliedstress0(3,1), appliedstress0(3,2), appliedstress0(3,3)
            read(10, fmt='(A)') tmp
            read(10, *) a
            read(10, fmt='(A)') tmp
            read(10, *) corefactor
        else
            write(*,1040) ierror
            1040 format(' ','Error opening file: IOSTAT = ',I6)
            read(*,*)
            stop
        end if      
        close(10)
        ! normalizing the slip plane normal vector
        sv = sv/norm(sv)
        
        if (analysis .EQ. 'PARTIALS') Nnodes=Nnodes*2

        end subroutine init_params 
    ! ###################    

end module param
