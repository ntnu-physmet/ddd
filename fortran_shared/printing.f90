module printing
  
  implicit none
  
contains
  
  !============================================================
  ! writing plotting script for gnuplot to file
  !===========================================================

  subroutine mk_script(Nstep, Lxmin, Lxmax, Lymin, Lymax, id_file)
    use param
    implicit none

    integer ::  nstep, iout, id_file
    real (kind=8) :: Lxmin, Lxmax, Lymin, Lymax
    
    intent(in) :: nstep, Lxmin, Lxmax, Lymin, Lymax, id_file
    
    !open (id_file,file="gnuscript")
    write(id_file,*)"gnuplot<<EOF"
    write(id_file,*)"unset title"
    write(id_file,*)"unset key"
!    write(id_file,'(A9,1p,E10.3,A2,E10.3)') &
!         "set size", .77*(Lxmax-Lxmin)/(Lymax-Lymin),",",1.
    write(id_file,'(A12,1p,E10.3,A1,E10.3,A1)')"set xrange [", Lxmin, ":", Lxmax,"]"
    write(id_file,'(A12,1p,E10.3,A1,E10.3,A1)')"set yrange [", Lymin,":", Lymax,"]"
    !write(id_file,'(A86)') 'set term gif animate delay 2 size 1280,960 optimize font ''/Library/Fonts/Arial.ttf'' 20'
    write(id_file,'(A86)') 'set term gif animate delay 2 size 1280,960 font ''/Library/Fonts/Arial.ttf'' 20'
    write(id_file,*) 'set style line 1 lt -1 lw 4 pt 3 ps 2'
    write(id_file,*) 'set style line 2 lt  2 lw 4 pt 3 ps 2'
    write(id_file,*)"set view 315,315"
    write(id_file,*)"set output 'disl.gif'"
    
    iout=0
    do iout =0,Nstep-1, max(Nstep/50,1)
       if (analysis .EQ. 'PARTIALS') then
          write(id_file,'(A38,I5,A43,I5,A74,A64)') &
               'splot ''disl_line.dat'' us 1:2:3 index ', 2*iout, &
               ' w l ls 1, ''disl_line.dat'' us 1:2:3 index ', 2*iout+1, &
               ' w l ls 2, ''points.dat'' index 1 w p pointtype 6 pointsize 1 lc rgb ''black''', &
               ', ''points.dat'' index 2 w p pointtype 6 pointsize 1 lc rgb ''blue'''
       else 
          write(id_file,'(A38,I5,A74,A64)') &
               'splot ''disl_line.dat'' us 1:2:3 index ', iout, &
               ' w l ls 1, ''points.dat'' index 1 w p pointtype 6 pointsize 1 lc rgb ''black''', &
               ', ''points.dat'' index 2 w p pointtype 6 pointsize 1 lc rgb ''blue'''
       endif
       enddo
    Write(id_file,'(A3)')"EOF"
  end subroutine mk_script
  ! ###################  
  
  
  !============================================================
  ! writing plotting script for gnuplot to file
  !===========================================================

  subroutine mk_script2(Nstep, Lzmin, Lzmax, Lxmin, Lxmax, id_file)
    use param
    implicit none

    ! plots 2D animation
    integer ::  nstep, iout, id_file
    real (kind=8) :: Lxmin, Lxmax, Lzmin, Lzmax
    
    intent(in) :: nstep, Lxmin, Lxmax, Lzmin, Lzmax, id_file
    
    !open (id_file,file="gnuscript2")
    write(id_file,*)"gnuplot<<EOF"
    write(id_file,*)"unset title"
    write(id_file,*)"unset key"
    write(id_file,'(A12,1p,E10.3,A1,E10.3,A1)')"set xrange [", Lzmin, ":", Lzmax,"]"
    write(id_file,'(A12,1p,E10.3,A1,E10.3,A1)')"set yrange [", Lxmin,":", Lxmax,"]"
    !write(id_file,'(A86)') 'set term gif animate delay 2 size 1280,960 optimize font ''/Library/Fonts/Arial.ttf'' 20'
    write(id_file,'(A86)') 'set term gif animate delay 1 size 1280,960 font ''/Library/Fonts/Arial.ttf'' 20'
    write(id_file,*) 'set style line 1 lt -1 lw 4 pt 3 ps 2'
    write(id_file,*) 'set style line 2 lt  2 lw 4 pt 3 ps 2'
    write(id_file,*)"set output 'disl2.gif'"
    
    iout=0
    !do iout =0,Nstep-1, max(Nstep/50,1)
    do iout =0,Nstep-1
       if (analysis .EQ. 'PARTIALS') then
          write(id_file,'(A38,I5,A43,I5,A74,A64)') &
               'plot ''disl_line.dat'' us 4:5 index ', 2*iout, &
               ' w l ls 1, ''disl_line.dat'' us 4:5 index ', 2*iout+1, &
               ' w l ls 2, ''points.dat'' index 1 w p pointtype 7 pointsize 1 lc rgb ''red''', &
               ', ''points.dat'' index 2 w p pointtype 6 pointsize 1 lc rgb ''blue'''
       else 
!          write(id_file,'(A38,I5,A74,A64)') &
!               'plot ''disl_line.dat'' us 4:5 index ', iout, &
!               ' w l ls 1, ''points.dat'' index 1 w p pointtype 7 pointsize 1 lc rgb ''red''', &
!               ', ''points.dat'' index 2 w p pointtype 6 pointsize 1 lc rgb ''blue'''
          write(id_file,'(A71,A68,A34,I5,A10)') &
               'plot ''points.dat'' index 1 w p pointtype 7 pointsize 1 lc rgb ''red'' ', &
               ', ''points.dat'' index 2 w p pointtype 6 pointsize 1 lc rgb ''blue''', &
               ', ''disl_line.dat'' us 4:5 index ', iout, ' w l ls 1'
       endif
    enddo
    Write(id_file,'(A3)')"EOF"
    Write(id_file,'(A50)')"gifsicle disl2.gif -O3 --colors 256 -o disl2.gif"
    
  end subroutine mk_script2
  ! ###################  
  
  
  !============================================================
  ! writing dislocation line to file
  !===========================================================

  subroutine prt_disl(rn, id_file)
    use param
    use utils
    implicit none

    integer ::  id_file, i
    real (kind=8) :: rn(Nntot,3), n_gl(3), z_gl, x_gl
    
    intent(in) :: id_file, rn
    
    n_gl = cross(sv,pervec)/norm(cross(sv,pervec))
    if (analysis .EQ. 'PARTIALS') then
       if(Bcond .EQ. 'FIX') then
          do i =fixed(1),fixed(2)
             z_gl = vdot(rn(i,:),pervec)
             x_gl = vdot(rn(i,:),n_gl)
             write(id_file,'(1p,5E14.4)') rn(i,3),rn(i,1), rn(i,2), z_gl, x_gl
          enddo
          write(id_file,*)
          write(id_file,*)
          do i =fixed(3),fixed(4)
             z_gl = vdot(rn(i,:),pervec)
             x_gl = vdot(rn(i,:),n_gl)
             write(id_file,'(1p,5E14.4)') rn(i,3),rn(i,1), rn(i,2), z_gl, x_gl
          enddo
          write(id_file,*)
          write(id_file,*)
       else
          ! remeshing with periodic partials are not implemented?
          do i =1,Nnodes/2
             z_gl = vdot(rn(i,:),pervec)
             x_gl = vdot(rn(i,:),n_gl)
             write(id_file,'(1p,5E14.4)') rn(i,3),rn(i,1), rn(i,2), z_gl, x_gl
          enddo
          write(id_file,*)
          write(id_file,*)
          do i =Nnodes/2 +1,Nnodes
             z_gl = vdot(rn(i,:),pervec)
             x_gl = vdot(rn(i,:),n_gl)
             write(id_file,'(1p,5E14.4)') rn(i,3),rn(i,1), rn(i,2), z_gl, x_gl
          enddo
          write(id_file,*)
          write(id_file,*)          
       endif
    else
       do i =1,Nnodes
          z_gl = vdot(rn(i,:),pervec)
          x_gl = vdot(rn(i,:),n_gl)
          write(id_file,'(1p,5E14.4)') rn(i,3),rn(i,1), rn(i,2), z_gl, x_gl
       enddo
       write(id_file,*)
       write(id_file,*)
    endif
  end subroutine prt_disl
  ! ###################  



  !============================================================
  ! writing solute positions to file
  !===========================================================

  subroutine prt_solutes(Psolutes, id_file)
    use param
    use utils
    implicit none

    integer ::  id_file, i
    real (kind=8) :: Psolutes(Nstot,3), n_gl(3), z_gl, x_gl, y_gl
    
    intent(in) :: Psolutes, id_file
    n_gl = cross(sv,pervec)/norm(cross(sv,pervec))
    
    do i =1,Nsolutes
       z_gl = vdot(Psolutes(i,:),pervec)
       x_gl = vdot(Psolutes(i,:),n_gl)
       y_gl = vdot(Psolutes(i,:),sv)
       write(id_file,'(5E14.4)') Psolutes(i,3), Psolutes(i,1), Psolutes(i,2), z_gl, x_gl
    enddo
    
    write(id_file,*)
    write(id_file,*)
    
    do i =1,Nsolutes
       z_gl = vdot(Psolutes(i,:),pervec)
       x_gl = vdot(Psolutes(i,:),n_gl)
       y_gl = vdot(Psolutes(i,:),sv)
       if (y_gl.gt.0.d0) &
            write(id_file,'(5E14.4)') Psolutes(i,3), Psolutes(i,1), Psolutes(i,2), z_gl, x_gl
    enddo
    
    write(id_file,*)
    write(id_file,*)
    
    do i =1,Nsolutes
       z_gl = vdot(Psolutes(i,:),pervec)
       x_gl = vdot(Psolutes(i,:),n_gl)
       y_gl = vdot(Psolutes(i,:),sv)
       if (y_gl.lt.0.d0) &
            write(id_file,'(5E14.4)') Psolutes(i,3), Psolutes(i,1), Psolutes(i,2), z_gl, x_gl
    enddo
  end subroutine prt_solutes
  ! ###################  
  
  
end module printing
