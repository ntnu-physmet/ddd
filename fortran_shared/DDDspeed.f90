    program DDDdepinning
    
    use param
    use utils
    use initialize
    use dynamics
    use topology
    use printing

    implicit none
    !Nsolutes = Area/L_s^2
    integer, parameter ::  id_file1 = 15, id_file2 = 16, id_file3 = 17, id_file4= 18, id_file5=19
    integer increased, irelax, itotrelax
    real(kind=8)    ::  rnew(Nntot,3), rn(Nntot,3), vn(Nntot,3), Psolutes(Nstot,3), & 
         dt, appliedstress(3,3),  cP(3), xave, &
         xave0, xave00, vave, rguess1(Nntot,3), xaveguess,xprnt
    !real(kind=8)    ::   distMat(Nntot, Nntot)
    integer         ::  links(Nntot-1,2), links3(Nntot,3), disl(Ndtot,2), curstep, &
         cnode,cele,nb_neigbs(Nntot), ele_neigbs(Nele_ngbs,Nele_ngbs)
    !integer         :: distMatID(Nntot, Nntot)
    integer         ::  iout, out_dinc, ioutstep,i,j,iprev, nx, ny, grid_pos(nntot,2)
!    integer :: r_grid(nngridtot,nngridtot,3), Ps_grid(nngridtot,nngridtot,Nsgridloc), &
!         nPS_grid(nngridtot,nngridtot)
    integer,allocatable :: r_grid(:,:,:), Ps_grid(:,:,:), nPS_grid(:,:)
    real(kind=8)    ::  tau, Lzmin, Lzmax, Lxmin, Lxmax, xavemax, timetot,rnd1, dx_sol
    logical         ::  remeshed, flag, convergent, iscoarsend, isrefined
    real(kind=8)    ::  xaveout,init_stress, vave0, vave_set

    real (kind=8) :: n_gl(3), z_gl, x_gl, y_gl

    
    allocate(r_grid(nngridtot,nngridtot,3), Ps_grid(nngridtot,nngridtot,Nsgridloc), &
         nPS_grid(nngridtot,nngridtot))

    
    open(id_file1, file="disl_line.dat")
    open(id_file2, file="points.dat")
    open(id_file3, file="gnuscript")
    open(id_file5, file="gnuscript2")
    open(id_file4, file="xpos.dat")

    ! remember rmaxstress = 20b in initialze, change if large splitting
    ! Remember seed in initialize!!!!!!!!!!!!!!!!!!!!!
    ! max stable time step for explicit Euler forward
    !dt0 = 0.5d0*((B(3)-A(3))/Nnodes)**2
    ! dt0=dt0*0.5d0
    write(*,*) "starting stress before increasing:"
    read(*,*)  init_stress
    write(*,*) "speed during test: "
    read(*,*)  vave_set
        
    dx_sol=10.
    
    out_dinc = 1
    timetot  = 0.
    xprnt=0.
    call init_params
    ioutstep = 5000
    Lzmin=bounds(1)
    Lzmax=bounds(2)

    Lxmin=bounds(3)
    Lxmax=bounds(4)
    xavemax=(Lxmax-Lxmin)-15.

    call init_disloc(rn, links, links3, disl)
    rmaxstress=0.
    call ele_ngbrs(rn,links3,nb_neigbs,ele_neigbs) 

    call init_solutes(Psolutes)

    call solute_grid(Psolutes, r_grid, Ps_grid, nPS_grid, nx, ny, dx_sol)
    
    do i=1,Nnodes
       grid_pos(i,1) =1
       grid_pos(i,2) =1
    enddo
    rcmin = 20.
    iout=1
    call prt_solutes(Psolutes, id_file2)
    close(id_file2)
    call prt_disl(rn, id_file1)
    
    ! calculate average glide positon of the original line.
    xave0 = 0.d0
    do j = 1,Nnodes
       xave0 =xave0 + vdot(rn(j,:),cross(sv,pervec))
    enddo
    xave0 = xave0/Nnodes
    
    totalsteps = 100000000
    itotrelax = 1
    ! loop for increasing relaxation stresses
    xaveout = -1000.
    do irelax=1,itotrelax
       tau = init_stress
       do i=1,3
          do j=1,3
             appliedstress(i,j) = tau*(sv(j)*bv(i) + sv(i)*bv(j))
          enddo
       enddo
       
       ! loop to do relaxation with current tau
       do curstep = 1, totalsteps
          call update_gridpos(rn, r_grid, grid_pos, nx, ny, dx_sol)
          call integrate1(rn, links, links3, disl, nb_neigbs, ele_neigbs, Psolutes, &
               appliedstress, vn, dt, convergent, grid_pos, Ps_grid, nPS_grid)
          timetot=timetot+dt
          if (.NOT. convergent) then
             exit
          end if
          ! calculate average glide positon of the original line.
          xave = 0.d0
          do j = 1,Nnodes
             xave=xave + vdot(rn(j,:),cross(sv,pervec))
          enddo
          xave = xave/Nnodes
          vave = (xave-xave0)/dt
          xave0=xave
          
          if(curstep.gt.2) then
             tau = tau + 5.E-3*(-vave + vave_set)
             tau=max(tau,1.0E-9)
             do i=1,3
                do j=1,3
                   appliedstress(i,j) = tau*(sv(j)*bv(i) + sv(i)*bv(j))
                enddo
             enddo
          endif
          
          if((curstep/20000)*20000.eq.curstep) then
             write(*,*) curstep
             write(*,*) "relaxing x, v, tau : ",xave,vave, tau
          endif
          
          if(xave.ge.xaveout) then
             xaveout = xave + 0.1
             write(*,*) 
             write(*,*) "xaveout: x, v, tau,t : ",xave,vave, tau, timetot
             iout = iout+1
             call prt_disl(rn, id_file1)
             write(id_file4,*) xave, tau, vave, timetot
          endif

          if(xave.gt.bounds(4)*0.7) goto 795

       end do

   984 continue
       !print the relaxed configuration to file
       iout = iout+1
       call prt_disl(rn, id_file1)

       xave = 0.d0
       do j = 1,Nnodes
          xave=xave + vdot(rn(j,:),cross(sv,pervec))
       enddo
       xave = xave/Nnodes
       vave = (xave-xave0)/dt
       xave0=xave

       call redist(rn)

       write(id_file4,*) xave, tau

    end do

795 continue    
    Lzmin=bounds(1)
    Lzmax=bounds(2)
    Lxmin=bounds(3)
    Lxmax=bounds(4)

    call mk_script(iout, Lzmin, Lzmax, Lxmin, Lxmax, id_file3) 
    
    Lzmin=bounds(1)
    Lzmax=bounds(2)

    Lxmin=bounds(3)
    Lxmax=bounds(4)

    call mk_script2(iout, Lzmin, Lzmax, Lxmin, Lxmax, id_file5) 

    write(*,*) "Finished succesfully!"
    write(*,*) rn(Nnodes/2,:)
    write(*,*) dt0
  
    close(id_file1)
    !close(id_file2)
    close(id_file3)
    close(id_file4)
    close(id_file5)
    
    end program DDDdepinning
    
