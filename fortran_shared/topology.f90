module topology
    
    contains
    
        subroutine connect(rn, links3, disl, node, ele)
   
        use param
        use utils
        implicit none
        
        real(kind=8)    ::  rn(Nntot,3), rntmp(Nntot,3)
        integer         ::  disl(Ndtot,2), disltmp(Ndtot,2), node, ele, i, Dnew, tmp, &
                            d1, d2, links3(Nntot,3), Nnodes_new, Ndisl_new, fixed_nodes(2)

        intent(inout)   ::  rn, disl, links3, node, ele
        
        ! which dislocation meets other dislocation
        d1 = 0
        d2 = 0
        do i = 1, Ndisl
            if (d1*d2 .EQ. 0) then
                if ((node .GE. disl(i,1)) .AND. (node .LE. disl(i,2))) then
                    d1 = i
                end if
                if ((ele .GE. disl(i,1)) .AND. (ele .LE. disl(i,2))) then
                    d2 = i
                end if
            else
                exit
            end if
        end do
        
        if (d1 .EQ. d2) then
            !***** split dislocation into two dislocations *****!
            if (ele .GT. node) then
                ! number of nodes in new dislocation
                Dnew  = ele-node
                ! update the links
                links3(disl(d1,1),1)          = disl(d1,2)-Dnew-1
                links3(disl(d1,2)-Dnew-1,3)   = disl(d1,1)
                links3(disl(d1,2)-Dnew,1)     = disl(d1,2)-1
                links3(disl(d1,2)-1,3)        = disl(d1,2)-Dnew
                links3(disl(d1,2):Nnodes-1,:) = links3(disl(d1,2)+1:Nnodes,:)-1
                links3(Nnodes,:)              = 0
                ! update the rn vector
                rntmp = rn
                rntmp(node:disl(d1,2)-Dnew-1,:)       = rn(ele+1:disl(d1,2),:)
                rntmp(disl(d1,2)-Dnew:disl(d1,2)-1,:) = rn(node+1:node+Dnew,:) 
                rntmp(disl(d1,2):Nnodes-1,:)          = rn(disl(d1,2)+1:Nnodes,:)
                rntmp(Nnodes,:)                       = 0.d0
                rn = rntmp

                ! update the fixed array
!                do i=1, size(fixed)
!                    if (fixed(i) .LT. node) then
!                        continue
!                    else if ( (fixed(i) .GT. node) .AND. (fixed(i) .LE. ele) ) then
!                        fixed(i) = fixed(i) + (disl(d1,2)-Dnew-node-1)
!                    else if ( (fixed(i) .GT. ele) .AND. (fixed(i) .LE. disl(d1,2)) ) then
!                        fixed(i) = fixed(i) - (ele+1-node)
!                    else
!                        fixed(i) = fixed(i) - 1 
!                    end if 
!                end do   

            else
                ! number of nodes in new dislocation
                Dnew  = node-ele-1
                ! update the links
                links3(disl(d1,1),1)          = disl(d1,2)-Dnew-1
                links3(disl(d1,2)-Dnew-1,3)   = disl(d1,1)
                links3(disl(d1,2)-Dnew,1)     = disl(d1,2)-1
                links3(disl(d1,2)-1,3)        = disl(d1,2)-Dnew
                links3(disl(d1,2):Nnodes-1,:) = links3(disl(d1,2)+1:Nnodes,:)-1
                links3(Nnodes,:)              = 0
                ! update the rn vector
                rntmp = rn
                rntmp(ele+1:disl(d1,2)-Dnew-1,:)      = rn(node+1:disl(d1,2),:)
                rntmp(disl(d1,2)-Dnew:disl(d1,2)-1,:) = rn(ele+1:ele+Dnew,:) 
                rntmp(disl(d1,2):Nnodes-1,:)          = rn(disl(d1,2)+1:Nnodes,:)
                rntmp(Nnodes,:)                       = 0.d0
                rn = rntmp

                ! update the fixed array
 !               do i=1, size(fixed)
 !                   if (fixed(i) .LT. (ele+1)) then
 !                       continue
 !                   else if ( (fixed(i) .GE. (ele+1)) .AND. (fixed(i) .LT. node) ) then
 !                       fixed(i) = fixed(i) + (disl(d1,2)-Dnew-ele-1)
 !                   else if ( (fixed(i) .GT. node) .AND. (fixed(i) .LE. disl(d1,2)) ) then
 !                       fixed(i) = fixed(i) - (node-ele)
 !                   else
 !                       fixed(i) = fixed(i) - 1 
 !                   end if 
 !               end do               

            end if
            ! update disl
            disltmp = disl               
            disltmp(d1+1,:) = (/ disl(d1,2)-Dnew, disl(d1,2)-1 /) ! create new dislocation 
            disltmp(d1+2:Ndisl+1,:) = disl(d1+1:Ndisl,:)-1        ! update the start and end point of others dislocations
            disl = disltmp
            disl(d1,2) = disl(d1,2)-Dnew-1                        ! update the endpoint of the original disloc
            fixed = (/ 1, disl(d1, 2) /)                          ! since this endpoint has potentially prescirbed fixed BC, tranfer it to new end point

            
            ! update number of dislocation
            Ndisl = Ndisl + 1
            ! update number of nodes
            Nnodes = Nnodes - 1      
        else
            !***** reconnect existing dislocations *****!
        end if
        
        end subroutine connect
        
        
        subroutine remesh(rn, links3, disl, iscoarsend, isrefined)
   
        use param
        use utils
        use dynamics
        implicit none
        
        real(kind=8)    ::  rn(Nntot,3), d12, d23, rn1(3), rn2(3), rn3(3), invR, nvec(3), tvec(3), cent(3), &
                            newnode(3), r12(3), r23(3), newnode2(3), r21(3), d21, r31(3), d31
        integer         ::  disl(Ndtot,2), links3(Nntot,3), d1, Nnewnodes, i
        logical         ::  iscoarsend, isrefined, refl1, refl2
        
        intent(out)     ::  iscoarsend, isrefined
        intent(inout)   ::  rn, disl, links3
        
        iscoarsend = .FALSE.
        ! number of newely added nodes
        Nnewnodes = 0
        
        ! COARSENING
        ! check the lengths criterion of all elements 
        i = 0
        do
            i = i + 1
            if (i .GT. Nnodes) then
                exit
            else if (isfixed(links3(i,2)) .EQV. .TRUE.) then
                cycle   ! never remove nodes with fixed attribute
            end if
            rn1 = rn(links3(i,1), :)
            rn2 = rn(links3(i,2), :)
            rn3 = rn(links3(i,3), :)
            if (Bcond .EQ. 'PER') then
                call reflect3p(rn1, rn2, rn3)
            end if
            r23 = rn3-rn2
            d23 = norm(r23)
            r21 = rn1-rn2
            d21 = norm(r21)
            r31 = rn1-rn3
            d31 = norm(r31)

            if (d31 .LT. 0.9d0*lmax) then
                call circle3P(rn1, rn2, rn3, invR, nvec, tvec)
                !if (invR .LT. 1.d0/lmax) then       ! R > lmax
                    if (d21 .LT. lmin) then
                        call removenode(rn, links3, disl, i)
                        Nnewnodes = Nnewnodes - 1
                        Nnodes = Nnodes - 1
                        iscoarsend = .TRUE.
                    elseif (d23 .LT. lmin) then
                        call removenode(rn, links3, disl, i)
                        Nnewnodes = Nnewnodes - 1
                        Nnodes = Nnodes - 1
                        iscoarsend = .TRUE.
                    end if
                !end if
            end if

            ! strict condition on too sharp angle
            if (abs(vdot(r23,r21)/(d23*d21) - 1.d0) .LT. 0.01) then
                call removenode(rn, links3, disl, i)
                Nnewnodes = Nnewnodes - 1
                Nnodes = Nnodes - 1
                iscoarsend = .TRUE.
            end if
        end do

        if (iscoarsend .EQV. .TRUE.) then
            write(*,*) 'coarsened'
        end if


        ! REFINING
        ! check the lengths criterion of all elements 
        isrefined = .FALSE.
        do i = 1, Nnodes
            rn1 = rn(links3(i,1), :)
            rn2 = rn(links3(i,2), :)
            rn3 = rn(links3(i,3), :)
            if (Bcond .EQ. 'PER') then
                call reflect3p(rn1, rn2, rn3)
            end if
            if ( isfixed(links3(i,2)) .AND. isfixed(links3(i,3)) ) then
                cycle                
            end if
            r23 = rn3-rn2
            d23 = norm(r23)
            
            if (d23 .GT. lmax) then
                if (isfixed(links3(i,2)) .EQV. .TRUE.) then   ! if i has fixed attribute
                    rn1 = rn(links3(links3(i,3),1), :)
                    rn2 = rn(links3(links3(i,3),2), :)
                    rn3 = rn(links3(links3(i,3),3), :)
                    if (Bcond .EQ. 'PER') then
                        call reflect3p(rn1, rn2, rn3)
                    end if
                    call gennode(rn1, rn2, rn3, 1, newnode, refl1)
                else if ((isfixed(links3(i,3)) .EQV. .TRUE.)) then
                    call gennode(rn1, rn2, rn3, 2, newnode, refl1)
                else
                    call gennode(rn1, rn2, rn3, 2, newnode, refl1)
                    rn1 = rn(links3(links3(i,3),1), :)
                    rn2 = rn(links3(links3(i,3),2), :)
                    rn3 = rn(links3(links3(i,3),3), :)
                    if (Bcond .EQ. 'PER') then
                        call reflect3p(rn1, rn2, rn3)
                    end if
                    call gennode(rn1, rn2, rn3, 1, newnode2, refl2)
                    ! if both newnode and newnode2 got reflected by gennode, than they are appart each other
                    ! and need to be put together to calcualte average
                    if ((refl1 .EQV. .TRUE.) .AND. (refl2 .EQV. .TRUE.)) then
                        if ( (abs(abs(vdot(newnode,pervec))-0.5d0*boxsize)) .LT. &  ! if newnode is closer to periodic boundary than newnode2
                             (abs(abs(vdot(newnode2,pervec))-0.5d0*boxsize)) ) then ! than reflect it out
                            call reflect(newnode)
                        else 
                            call reflect(newnode2)
                        end if
                    end if

                    newnode = 0.5d0*(newnode + newnode2)
                end if

                call addnode(rn, links3, disl, i, newnode)
                Nnewnodes = Nnewnodes + 1
                Nnodes = Nnodes + 1
                isrefined = .TRUE.
                
            end if
        end do
        
        end subroutine remesh
        
        
        subroutine redist(rn)
          ! redistribute the nodes of periodic partials.
          ! called after each t incr. so that the nodes are shifted within segments to the right or left.
        use param
        use utils
        use dynamics
        implicit none
        
        real(kind=8)    ::  rn(Nntot,3), rntmp(Nntot,3), dz, z1, z2, &
             rn1(3), rn2(3), rn3(3), fac, ztarget, ld1, ld2, dl, dr
        integer         ::  i
        
        intent(inout)   ::  rn
        
        
        ! only for periodic bc
        if(Bcond .ne. 'PER') return
        ! check the lengths criterion of all elements 
        if (analysis .EQ. 'PARTIALS') then
           dz = boxsize/(Nnodes/2.d0)
           rntmp = rn
           
           ld1 = 0.
           ld2 = 0.
           
           ! node 1, first partial
           rn1 = rntmp(1,:)
           ztarget = -boxsize*0.5d0 +dz*0.5d0
           z1 = vdot(rn1,pervec)
           if (z1.lt.ztarget) then
              rn2 = rntmp(Nnodes/2,:)
              call reflect(rn2)
           else
              rn2 = rntmp(2,:)
           endif
           z2 = vdot(rn2,pervec)
           fac  = (-z1 +ztarget)/(z2-z1)
           rn(1,:) = rn1 + fac*(rn2-rn1)
           
           ! node Nnodes/2, first partial rhs
           rn1 = rntmp(Nnodes/2,:)
           ztarget = boxsize*0.5d0 -dz*0.5d0
           z1 = vdot(rn1,pervec)
           if (z1.gt.ztarget) then
              rn2 = rntmp(1,:)
              call reflect(rn2)
           else
              rn2 = rntmp(Nnodes/2-1,:)
           endif
           z2 = vdot(rn2,pervec)
           fac  = (-z1 +ztarget)/(z2-z1)
           rn(Nnodes/2,:) = rn1 + fac*(rn2-rn1)
           
           
           do i = 2, Nnodes/2
              rn1 = rntmp(i-1,:)
              rn2 = rntmp(i,:)
              ld1 = ld1 + norm(rn2-rn1)
           end do
           dl = ld1/(Nnodes/2-1.)
           
           do i = 2, Nnodes/2-1
              rn1 = rn(i-1,:)
              rn2 = rn(i,:)
              rn3 = rn(i+1,:)
              dr= norm(rn2-rn1)
              if (dr.lt.dl) then
                 rn(i,:) = rn1 + dl*(rn2-rn1)/dr
              else
                 rn(i,:) = rn2 + (dl-dr)*(rn3-rn2)/norm(rn3-rn2)
              endif
           enddo

        !do i = 2, Nnodes/2-1
        !   ztarget = -boxsize*0.5d0 +dz*(i-1) +dz*0.5d0
        !   rn1 = rntmp(i,:)
        !   z1 = vdot(rn1,pervec)
        !   if (z1.lt.ztarget) then
        !      rn2 = rntmp(i+1,:)
        !   else
        !      rn2 = rntmp(i-1,:)
        !   endif
        !   z2 = vdot(rn2,pervec)
        !   fac  = (-z1 +ztarget)/(z2-z1)
        !   rn(i,:) = rn1 + fac*(rn2-rn1)
        !end do

        ! SECOND PARTIAL
        ! node Nnodes/2+1, second partial
           rn1 = rntmp(Nnodes/2+1,:)
           ztarget = -boxsize*0.5d0 +dz*0.5d0
           z1 = vdot(rn1,pervec)
           if (z1.lt.ztarget) then
              rn2 = rntmp(Nnodes,:)
              call reflect(rn2)
           else
              rn2 = rntmp(Nnodes/2+2,:)
           endif
           z2 = vdot(rn2,pervec)
           fac  = (-z1 +ztarget)/(z2-z1)
           rn(Nnodes/2+1,:) = rn1 + fac*(rn2-rn1)
           
           ! node Nnodes, second partial rhs
           rn1 = rntmp(Nnodes,:)
           ztarget = boxsize*0.5d0 -dz*0.5d0
           z1 = vdot(rn1,pervec)
           if (z1.gt.ztarget) then
              rn2 = rntmp(Nnodes/2+1,:)
              call reflect(rn2)
           else
              rn2 = rntmp(Nnodes-1,:)
           endif
           z2 = vdot(rn2,pervec)
           fac  = (-z1 +ztarget)/(z2-z1)
           rn(Nnodes,:) = rn1 + fac*(rn2-rn1)
           
           
           do i = Nnodes/2+2,Nnodes
              rn1 = rntmp(i-1,:)
              rn2 = rntmp(i,:)
              ld2 = ld2 + norm(rn2-rn1)
           end do
           dl = ld2/(Nnodes-(Nnodes/2+1.))
           
           
           do i = Nnodes/2+2, Nnodes-1
              rn1 = rn(i-1,:)
              rn2 = rn(i,:)
              rn3 = rn(i+1,:)
              dr= norm(rn2-rn1)
              if (dr.lt.dl) then
                 rn(i,:) = rn1 + dl*(rn2-rn1)/dr
              else
                 rn(i,:) = rn2 + (dl-dr)*(rn3-rn2)/norm(rn3-rn2)
              endif
           enddo
        
        !do i = Nnodes/2+2, Nnodes-1
        !   ztarget = -boxsize*0.5d0 +dz*(i-Nnodes/2-1) +0.5d0*dz
        !   rn1 = rntmp(i,:)
        !   z1 = vdot(rn1,pervec)
        !   if (z1.lt.ztarget) then
        !      rn2 = rntmp(i+1,:)
        !   else
        !      rn2 = rntmp(i-1,:)
        !   endif
        !   z2 = vdot(rn2,pervec)
        !   fac  = (-z1 +ztarget)/(z2-z1)
        !   rn(i,:) = rn1 + fac*(rn2-rn1)
        !end do

        else ! not partials, only one
           dz = boxsize/(Nnodes)
           rntmp = rn
           
           ld1 = 0.
           
           ! node 1
           rn1 = rntmp(1,:)
           ztarget = -boxsize*0.5d0 +dz*0.5d0
           z1 = vdot(rn1,pervec)
           if (z1.lt.ztarget) then
              rn2 = rntmp(Nnodes/2,:)
              call reflect(rn2)
           else
              rn2 = rntmp(2,:)
           endif
           z2 = vdot(rn2,pervec)
           fac  = (-z1 +ztarget)/(z2-z1)
           rn(1,:) = rn1 + fac*(rn2-rn1)
           
           ! node Nnodes,rhs
           rn1 = rntmp(Nnodes,:)
           ztarget = boxsize*0.5d0 -dz*0.5d0
           z1 = vdot(rn1,pervec)
           if (z1.gt.ztarget) then
              rn2 = rntmp(1,:)
              call reflect(rn2)
           else
              rn2 = rntmp(Nnodes-1,:)
           endif
           z2 = vdot(rn2,pervec)
           fac  = (-z1 +ztarget)/(z2-z1)
           rn(Nnodes,:) = rn1 + fac*(rn2-rn1)
           
           
           do i = 2, Nnodes
              rn1 = rntmp(i-1,:)
              rn2 = rntmp(i,:)
              ld1 = ld1 + norm(rn2-rn1)
           end do
           dl = ld1/(Nnodes-1.)
           
           do i = 2, Nnodes-1
              rn1 = rn(i-1,:)
              rn2 = rn(i,:)
              rn3 = rn(i+1,:)
              dr= norm(rn2-rn1)
              if (dr.lt.dl) then
                 rn(i,:) = rn1 + dl*(rn2-rn1)/dr
              else
                 rn(i,:) = rn2 + (dl-dr)*(rn3-rn2)/norm(rn3-rn2)
              endif
           enddo
        endif

           
        end subroutine redist
        



        subroutine removenode(rn, links3, disl, remnode)

        use param
        use utils
        implicit none
        
        real(kind=8)    ::  rn(Nntot,3), d12, d23, newnode(3)
        integer         ::  disl(Ndtot,2), remnode, links3(Nntot,3), d1, i
        
        intent(in)      ::  remnode
        intent(inout)   ::  rn, disl, links3

        ! identify the dislocation id containing ele
        d1 = 0
        do i = 1, Ndisl
            if (d1 .EQ. 0) then
                if ((remnode .GE. disl(i,1)) .AND. (remnode .LE. disl(i,2))) then
                    d1 = i
                end if
            else
                exit
            end if
        end do
        ! update links3
        links3(disl(d1,2)-1,3) = links3(disl(d1,2),3)
        links3(disl(d1,1),1) = links3(disl(d1,1),1) - 1
        links3(disl(d1,2):Nnodes-1,:) = links3(disl(d1,2)+1:Nnodes,:) - 1
        links3(Nnodes,:) = 0
        ! update rn
        rn(remnode:Nnodes-1,:) = rn(remnode+1:Nnodes,:)
        rn(Nnodes,:) = 0.d0
        ! update disl
        disl(d1,2) = disl(d1,2) - 1
        disl(d1+1:Ndisl,:) = disl(d1+1:Ndisl,:) - 1
        ! update fixed array
        where (fixed .GT. remnode) fixed = fixed - 1

        end subroutine removenode

      
        subroutine addnode(rn, links3, disl, ele, newnode)
        
        use param
        use utils
        implicit none
        
        real(kind=8)    ::  rn(Nntot,3), d12, d23, newnode(3)
        integer         ::  disl(Ndtot,2), ele, links3(Nntot,3), d1, i
        
        intent(in)      ::  newnode, ele
        intent(inout)   ::  rn, disl, links3
        
        ! identify the dislocation id containing ele
        d1 = 0
        do i = 1, Ndisl
            if (d1 .EQ. 0) then
                if ((ele .GE. disl(i,1)) .AND. (ele .LE. disl(i,2))) then
                    d1 = i
                end if
            else
                exit
            end if
        end do
        ! update links3
        links3(disl(d1,2),3) = links3(disl(d1,2),2) + 1
        links3(disl(d1,1),1) = links3(disl(d1,1),1) + 1
        links3(disl(d1,2)+2:Nnodes+1,:) = links3(disl(d1,2)+1:Nnodes,:) + 1
        links3(disl(d1,2)+1,:) = (/ disl(d1,2), disl(d1,2)+1, disl(d1,1) /)
        ! update rn
        rn(ele+2:Nnodes+1,:) = rn(ele+1:Nnodes,:)
        rn(ele+1,:) = newnode
        ! update disl
        disl(d1,2) = disl(d1,2) + 1
        disl(d1+1:Ndisl,:) = disl(d1+1:Ndisl,:) + 1
        ! update fixed array
        where (fixed .GT. ele) fixed = fixed + 1

        end subroutine addnode
        
end module topology
