    program DDDeps
    
    use param
    use utils
    use initialize
    use dynamics
    use topology
    use printing

    implicit none
    !Nsolutes = Area/L_s^2
    integer, parameter ::  id_file1 = 15, id_file2 = 16, id_file3 = 17, id_file4= 18, id_file5=19

    real(kind=8)    ::  rnew(Nntot,3), rn(Nntot,3), vn(Nntot,3), Psolutes(Nstot,3), & 
         dt, appliedstress(3,3), distMat(Nntot, Nntot), cP(3), xave, &
         xave0, xave00, vave, rguess1(Nntot,3), xaveguess
    integer         ::  links(Nntot-1,2), links3(Nntot,3), disl(Ndtot,2), curstep, &
         distMatID(Nntot, Nntot), cnode, cele
    integer         ::  iout, out_dinc, ioutstep,i,j,ieps
    real(kind=8)    ::  tau, Lzmin, Lzmax, Lxmin, Lxmax, xavemax, timetot
    logical         ::  remeshed, flag, convergent, iscoarsend, isrefined

    open(id_file1, file="tau_eps.dat")
    close(id_file1)

    ! remember rmaxstress = 20b in initialze, change if large splitting
      
    ! max stable time step for explicit Euler forward
    !dt0 = 0.5d0*((B(3)-A(3))/Nnodes)**2
   ! dt0=dt0*0.5d0
    out_dinc = 1
    timetot  = 0.
    call init_params
    ioutstep = 300
    Lzmin=bounds(1)
    Lzmax=bounds(2)

    Lxmin=bounds(3)
    Lxmax=bounds(4)
    call init_solutes(Psolutes)


    xavemax=(Lxmax-Lxmin)-5.

    ! START LOOP ON the misfit parameter
    do ieps=1,10
       
    solGRKdRvec(4) = 0.01+0.03*ieps
    call init_disloc(rn, links, links3, disl)
  
    rcmin = 20.
        
    ! calculate average glide positon of the original line.
    xave0 = 0.d0
    do j = 1,Nnodes
       xave0 =xave0 + vdot(rn(j,:),cross(sv,pervec))
    enddo
    xave0 = xave0/Nnodes
    
    tau = 0.d0
    do i=1,3
       do j=1,3
          appliedstress(i,j) = tau*(sv(j)*bv(i) + sv(i)*bv(j))
       enddo
    enddo


    ! main time loop
    xave00= xave0

    tau = 5.D-5
    do i=1,3
       do j=1,3
          appliedstress(i,j) = tau*(sv(j)*bv(i) + sv(i)*bv(j))
       enddo
    enddo

    totalsteps = 10000000
    iout=1  
    
    do curstep = 1, totalsteps
       call integrate(rn, links, links3, disl, distMat, distMatID, Psolutes, appliedstress, vn, dt, convergent)
       timetot=timetot+dt
       if (.NOT. convergent) then
          exit
       end if
       ! calculate average glide positon of the original line.
       xave = 0.d0
       do j = 1,Nnodes
          xave=xave + vdot(rn(j,:),cross(sv,pervec))
       enddo
       xave = xave/Nnodes
       vave = (xave-xave0)/dt
       xave0=xave

       call redist(rn)

       if(curstep .eq. (curstep/ioutstep)*ioutstep) then
          iout = iout + 1
          write(*,*) curstep
          write(*,*) "x, v : ",xave,vave
       endif

       if(vave.lt.1.d-3) then
          rguess1=rn
          xaveguess=xave
          tau=tau*1.02
          write(*,*) "x,tau,vave: ",xave,tau, vave
       endif
       do i=1,3
          do j=1,3
             appliedstress(i,j) = tau*(sv(j)*bv(i) + sv(i)*bv(j))
          enddo
       enddo
       if((xave-xave00).gt.xavemax) exit
    end do


    rn    = rguess1
    xave0 = xaveguess
    tau   = tau*0.5
    do i=1,3
       do j=1,3
          appliedstress(i,j) = tau*(sv(j)*bv(i) + sv(i)*bv(j))
       enddo
    enddo
    do curstep = 1, totalsteps
       call integrate(rn, links, links3, disl, distMat, distMatID, Psolutes, appliedstress, vn, dt, convergent)
       timetot=timetot+dt
       if (.NOT. convergent) then
          exit
       end if
       ! calculate average glide positon of the original line.
       xave = 0.d0
       do j = 1,Nnodes
          xave=xave + vdot(rn(j,:),cross(sv,pervec))
       enddo
       xave = xave/Nnodes
       vave = (xave-xave0)/dt
       xave0=xave
       call redist(rn)
       if(curstep .eq. (curstep/ioutstep)*ioutstep) then
          iout = iout + 1
          write(*,*) curstep
          write(*,*) "x, v : ",xave,vave
       endif

       if(vave.lt.1.d-6) then
          tau=tau*1.02
          write(*,*) "x,tau,vave: ",xave,tau, vave
       endif
       do i=1,3
          do j=1,3
             appliedstress(i,j) = tau*(sv(j)*bv(i) + sv(i)*bv(j))
          enddo
       enddo
       if((xave-xave00).gt.xavemax+4.) exit
       if(xave.gt.xaveguess + 3.) exit
    end do

    open(id_file1, file= "tau_eps.dat", position= 'append')
    write(id_file1,*) solGRKdRvec(4), tau
    close(id_file1)
    
    write(*,*) solGRKdRvec(4), tau
    write(*,*)
    enddo
    
    write(*,*) "Finished succesfully!"
    write(*,*) rn(Nnodes/2,:)
  
    close(id_file1)
    
    end program DDDeps
    
