    program DDD
    
    use param
    use utils
    use initialize
    use dynamics
    use topology
    use printing

    implicit none
    !Nsolutes = Area/L_s^2
    integer, parameter ::  id_file1 = 15, id_file2 = 16, id_file3 = 17, id_file4= 18, id_file5=19

    real(kind=8)    ::  rnew(Nntot,3), rn(Nntot,3), vn(Nntot,3), Psolutes(Nstot,3), & 
         dt, appliedstress(3,3),  cP(3), xave, &
         xave0, xave00, vave, rguess1(Nntot,3), xaveguess,xprnt,xavemx, tmx,vavemx
    !real(kind=8)    ::  distMat(Nntot,Nntot)
    integer         ::  links(Nntot-1,2), links3(Nntot,3), disl(Ndtot,2), curstep, &
         cnode,cele,nb_neigbs(Nntot),ele_neigbs(Nele_ngbs,Nele_ngbs)
    !integer         ::  distMatID(Nntot, Nntot)
    integer         ::  iout, out_dinc, ioutstep,i,j,iprev, nx, ny, grid_pos(nntot,2),&
         r_grid(nngridtot,nngridtot,3), Ps_grid(nngridtot,nngridtot,Nsgridloc), &
         nPS_grid(nngridtot,nngridtot), istop,stepold, redistgrid
    real(kind=8)    ::  tau, Lzmin, Lzmax, Lxmin, Lxmax, xavemax, timetot,rnd1, dx_sol, &
         S_offset, xrange, yrange, glidedir(3),bvnormal(3),vtotave
    logical         ::  remeshed, flag, convergent, iscoarsend, isrefined

    open(id_file1, file="disl_line.dat")
    open(id_file2, file="points.dat")
    open(id_file3, file="gnuscript")
    open(id_file5, file="gnuscript2")
    open(id_file4, file="xpos.dat")

    ! distance in grid (for bookkeeping the solutes)

    dx_sol=10.
    redistgrid=1
    timetot  = 0.
    iout = 0
    call init_params
    ioutstep = 5000
    Lzmin=bounds(1)
    Lzmax=bounds(2)

    Lxmin=bounds(3)
    Lxmax=bounds(4)
    
    call init_disloc(rn, links, links3, disl)
    !rmaxstress is given in init_disloc
    rmaxstress=0.
    call ele_ngbrs(rn,links3,nb_neigbs,ele_neigbs) 
    !call init_solutes(Psolutes)
    !
    ! Generate one solute point
    !
    Nsolutes=1
    xrange = bounds(2) - bounds(1)
    yrange = bounds(4) - bounds(3)
    glidedir = -cross(pervec,sv)/norm(cross(pervec,sv))
    bvnormal = cross(bv,sv)/norm(cross(bv,sv))
    S_offset = sqrt(6.d0)/2.d0*norm(bv)
    
    !Psolutes(1,:) = bv*i_b +bvnormal*i_g*sqrt(3.d0)*.5d0 + sv*S_offset
    Psolutes(1,:) =  sv*S_offset + glidedir*3.
    ! END one point
    !
    call solute_grid(Psolutes, r_grid, Ps_grid, nPS_grid, nx, ny, dx_sol)
    
    do i=1,Nnodes
       grid_pos(i,1) =1
       grid_pos(i,2) =1
    enddo
    rcmin = 20.
    call prt_solutes(Psolutes, id_file2)
    call prt_disl(rn, id_file1)
    iout = iout+1
    ! calculate average glide positon of the original line.
    xave0 = 0.d0
    do j = 1,Nnodes
       xave0 =xave0 + vdot(rn(j,:),cross(sv,pervec))
    enddo
    xave0 = xave0/Nnodes

    ! appliedstress0 from param
    do i=1,3
       do j=1,3
          appliedstress = appliedstress0
       enddo
    enddo

    totalsteps = 10000000
    istop = 0
    stepold = 0
    !dt0=dt0*0.5d0

    xavemx=0.
    tmx = 0.d0
    
    do curstep = 1, totalsteps
       if(redistgrid.eq.1) &
            call update_gridpos(rn, r_grid, grid_pos, nx, ny, dx_sol)
       call integrate1(rn, links, links3, disl, nb_neigbs, ele_neigbs, Psolutes, &
            appliedstress, vn, dt, convergent, grid_pos, Ps_grid, nPS_grid)
       timetot=timetot+dt
       if (.NOT. convergent) then
          exit
       end if
       ! calculate average glide positon of the original line.
       xave = 0.d0
       vtotave = 0.d0
       do j = 1,Nnodes
          xave=xave + vdot(rn(j,:),cross(sv,pervec))
          vtotave=vtotave + sqrt(vdot(vn(j,:),vn(j,:)))
       enddo
       xave = xave/Nnodes
       vave = (xave-xave0)/dt
       vtotave = vtotave/Nnodes
       xave0=xave
       vavemx=(xave-xavemx)/(timetot-tmx)
       if(xave.gt.xavemx)then
          xavemx = xave
          tmx    = timetot
       endif
       if((curstep/1200)*1200.eq.curstep) then
          iout = iout + 1
          call prt_disl(rn, id_file1)
          write(*,*) curstep
          write(*,*) " x, v, |v|,vmax, t : ",xave,vave, vtotave, vavemx, timetot
          write(id_file4,*) xave, tau
       endif

       !if(abs(vtotave).lt.1.d-3) redistgrid = 0
       !if(abs(vave).lt.1.d-6) then
       if(abs(vave).lt.1.d-9) then
          !if(stepold.eq.curstep-1) then
          !   istop = istop+1
          !else
          !   istop = 1
          !endif
          !stepold = curstep
          write(*,*) "x, |v|,vmax, istop: ",xave, vtotave, vavemx,istop,curstep
          !if(istop.ge.20) exit
          exit
       endif
    end do
    
    write(*,*) "x, v, |v|: ",xave, vave, vtotave
    iout = iout + 1
    call prt_disl(rn, id_file1)

    
    Lzmin=bounds(1)
    Lzmax=bounds(2)
    Lxmin=bounds(3)
    Lxmax=bounds(4)

    call mk_script(iout, Lzmin, Lzmax, Lxmin, Lxmax, id_file3) 
    
    Lzmin=bounds(1)
    Lzmax=bounds(2)

    Lxmin=bounds(3)
    Lxmax=bounds(4)

    call mk_script2(iout, Lzmin, Lzmax, Lxmin, Lxmax, id_file5) 

    write(*,*) "Finished succesfully!"
    write(*,*) rn(Nnodes/2,:)
    write(*,*) dt0
    !read(*,*)
  
    close(id_file1)
    close(id_file2)
    close(id_file3)
    close(id_file4)
    close(id_file5)
    
    end program DDD
    
