      program autocorr
      integer i, j, idisl,ifrom(1000),ito(1000),k,l
      double precision y,x,yave, y1, xx(1000),yy(1000)
      character*8 txt
      open(10,file="disl_line.dat")
      idisl=0
      ifrom(1)=1
      iflag=0
      do i = 1,10000000
         read(10,'(a)',END=100) txt
         if (len_trim(txt).eq.0) then
            read(10,*)
            idisl=idisl+1
            ito(idisl) = i-1+idisl-1
            ifrom(idisl+1)=i+2+idisl-1
         endif
      enddo
 100  continue
      
      close(10)
      
      open(11,file="disl_line.dat")
      do i = 1,idisl
         k=0
         yave=0.
         do j = ifrom(i),ito(i)
            k=k + 1
            read(11,*) xx(k),yy(k)
            yave=yave+yy(k)
         enddo
         read(11,*)
         read(11,*)
c
         yave=yave/k
c     integrate autocorrelation function as function of x_l
         open(12, file="autocorr.dat")
         do l=1,k
            autoc=0.
            do j = 1,k
               if(l+j.gt.k) then 
                  y1=yy(l+j-k)-yave
               else
                  y1=yy(l+j)-yave
               endif
               autoc = autoc + y1*(yy(j)-yave)
            enddo
c            autoc = autoc/400./800.
            autoc = autoc/800./1600.
            write(12,*) xx(l),autoc
c
         enddo
         write(12,*)
         write(12,*)
      enddo
      close(11)
      close(12)
      end
