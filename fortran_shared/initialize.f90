module initialize
   
contains    
  ! ###################    	    
  subroutine init_disloc(rn, links, links3, disl)
    
    use param
    use utils
    implicit none
    
    integer  ::  links(Nntot-1,2), links3(Nntot,3), i, disl(Ndtot, 2), Ndisl_new, &
         Nnodes_new, Nnodes1
    real(kind=8)  ::  rn(Nntot,3), ds, initlength, initoffset
    intent(out)   ::  rn, links, links3, disl
    
    ! Note: NNodes ->Nnodes*2 if partial, in param.
    if (analysis .EQ. 'PARTIALS') then
       Nnodes1 = Nnodes/2
    else
       Nnodes1 = Nnodes
    endif
    if(allocated(fixed)) deallocate(fixed)
    if(allocated(endpt)) deallocate(endpt)

    initoffset=min(boxsize*0.05, 5.d0)
    
    ! First assume not partials.
    if (Bcond .EQ. 'FIX') then
       initlength = boxsize/1.d0
       allocate(fixed(2), source=(/ 1, Nnodes1 /))
    else
        initlength = boxsize
        allocate(fixed(1), source=(/ -1 /))
    end if

    allocate(endpt(2), source=(/ 1, Nnodes1 /))

    ! assign the initial dislocation  (or first partial)
    rn = 0.d0
    If(Bcond.eq.'FIX') then
       ds = initlength/(Nnodes1 -1)
       rn(1,:)      = -0.5d0*initlength*pervec
    else
       ! start ds/2 from boundary, required for Bcond.eq.'PER'
       ds = initlength/Nnodes1
       rn(1,:)      = -0.5d0*initlength*pervec +0.5d0*ds*pervec
    endif
    links(1,:)   = (/ 1, 2 /)
    links3(1,:)  = (/ Nnodes1, 1, 2 /)
    
    do i = 2, Nnodes1-1
       rn(i,:)       = rn(i-1,:) + ds*pervec
       links(i,:)    = (/ i, i+1 /)
       links3(i,:)   = (/ i-1, i, i+1 /)
    end do
    
    rn(Nnodes1,:)       = rn(Nnodes1-1,:) + ds*pervec
    links3(Nnodes1,:)   = (/ Nnodes1-1, Nnodes1, 1 /)

    ! disl contains the first and the last node id of disl's in vectors as rn, vn, fn and links3
    disl(1, :) =  (/ 1, Nnodes1 /) 
    Ndisl = 1    
    
    if (analysis .EQ. 'PARTIALS') then
       ! calculation of bvtrail and bvlead are moved to param
       write(*,*) "Leading Shockley partial"
       write(*,*) bvlead
       write(*,*)
       write(*,*) "Trailing Shockley partial"
       write(*,*) bvtrail
       write(*,*)
       if(Bcond.eq.'FIX') then 
          rn(Nnodes1+1,:)      = -0.5d0*initlength*pervec
       Else 
          rn(Nnodes1+1,:)      = -0.5d0*initlength*pervec +0.5*ds*pervec
       Endif
       links(Nnodes1+1,:)   = Nnodes1 + (/ 1, 2 /)
       links3(Nnodes1+1,:)  = Nnodes1 + (/ Nnodes1, 1, 2 /)
       
       do i = Nnodes1+2, 2*Nnodes1-1
          rn(i,:)       = rn(i-1,:) + ds*pervec
          links(i,:)    = (/ i, i+1 /)
          links3(i,:)   = (/ i-1, i, i+1 /)
       end do
       
       rn(2*Nnodes1,:)       = rn(2*Nnodes1-1,:) + ds*pervec
       links3(2*Nnodes1,:)   = (/ 2*Nnodes1-1, 2*Nnodes1, Nnodes1+1 /)
       
       ! offset the second dislocation by some distance behind
       offset = 5.d0*cross(sv,pervec)
       
       do i=1, Nnodes1
          rn(Nnodes1+i,:) = rn(Nnodes1+i,:) + offset
       end do
       
       ! disl contains the first and the last node id of disl's in vectors as rn, vn, fn and links3
       disl(2, :) =  Nnodes1 + (/ 1, Nnodes1 /) 
       ! number of dislocations
       Ndisl = 2
       ! update number of nodes, changed, now done in param
       !Nnodes = 2*Nnodes1
       ! fix both partials
       if (Bcond .EQ. 'FIX') then
          deallocate(fixed)
          allocate(fixed(4), source = (/ 1, Nnodes/2, Nnodes/2+1, Nnodes /))
       !else !  put this here even if it already is set above
       !   deallocate(fixed)
       !   allocate(fixed(1), source=(/ -1 /))
       end if

       deallocate(endpt)
       allocate(endpt(4), source = (/ 1, Nnodes/2, Nnodes/2+1, Nnodes /))
    end if
    
    ! offset the dislocation by some distance
    offset = initoffset*cross(sv,pervec)
    
    do i=1, Nnodes
       rn(i,:) = rn(i,:) + offset
    end do   

    ! suggest minimum segment length lmin to be half of the element length
    lmin = ds/1.2d0
    ! suggest maximum segment length lmax to be 2 times the element length
    lmax = 2.d0*ds
    ! critical distance for connect reaction
    rcmin = ds/2.d0
    ! critical distance for stress calculation
    !rmaxstress = 10.d0*ds
    rmaxstress = 20.d0*sqrt(bv(1)**2+bv(2)**2+bv(3)**2)
    ! dislocation core parameter a'la Wei Cai, a = lmin/sqrt(3.d0)*0.5d0 but is instead set in param
    if(int_scheme .eq. 'EXP') then
       !dt0 = 0.5d0*(lmin*1.5)**2
       dt0 = 0.5d0*(lmin)**2
    endif
    
  end subroutine init_disloc
  ! ###################    	
  ! ###################    	

  ! ###################    	
  ! ###################    	
  subroutine init_solutes(Psolutes)
        
    use param
    use utils
    implicit none
    
    integer      ::  i, isze, ipos, i_b, i_g, j
    real(kind=8) ::  Psolutes(Nstot, 3), xrange, yrange, updown, &
         glidedir(3), S_offset, rnd1, rnd2, solpos(50000000,3), soltmp(3), bvnormal(3)

    real (kind=8) :: n_gl(3), z_gl, x_gl, y_gl
    
    intent(out)  ::  Psolutes
    
    Psolutes = 0.d0
    xrange = bounds(2) - bounds(1)
    yrange = bounds(4) - bounds(3)
    glidedir = -cross(pervec,sv)/norm(cross(pervec,sv))
    bvnormal = cross(bv,sv)/norm(cross(bv,sv))
    S_offset = sqrt(6.d0)/2.d0*norm(bv)

    ! count and initialize all potential solute positions in the glide plane
    !write(*,*) bv
    !write(*,*) bvnormal
    !read(*,*)
    ipos = 0
    do i_b = -10000,10000
       do i_g = -10000,10000
          soltmp = bv*i_b +bvnormal*i_g*sqrt(3.d0)*.5d0
          if((i_g/2)*2.eq.i_g) soltmp = soltmp +0.5d0*bv
          if((vdot(soltmp,pervec) .le. bounds(2)).and. &
               (vdot(soltmp,pervec) .ge. bounds(1)) &
               .and. (vdot(soltmp,glidedir) .le. bounds(4)) .and. &
               (vdot(soltmp,glidedir) .ge. bounds(3))) then
             ipos = ipos + 1
             solpos(ipos,:) = soltmp
!             write(*,*) soltmp(1),soltmp(2),soltmp(3)
             if(ipos.ge.50000000) then
                write(*,*) "too many solute atoms for memory"
                exit
             end if
             
          endif
       enddo
    enddo
    write(*,*) "zmin, zmax, xmin, xmax "
    write(*,*)  bounds(1), bounds(2), bounds(3), bounds(4)
    write(*,*) "c = ", (Nsolutes*1.)/(2.*ipos)
    !read(*,*)

    write(*,*) ipos*2
    do i = 1,ipos
       solpos(i+ipos,:)=solpos(i,:)+sv*S_offset
       solpos(i,:)=solpos(i,:)-sv*S_offset
    enddo
    ipos = ipos*2

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! same sequence each time without seeed!!
    ! call random_seed()

    ! code for same initial start of random series
    CALL RANDOM_SEED(SIZE=isze)
    CALL RANDOM_SEED(PUT=(/(1346, i = 1, isze)/))
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
    
    !call random_number(Psolutes(:Nsolutes,(/3,1/) ))
    !Psolutes(:,3) = bounds(1) + Psolutes(:,3)*xrange
    !Psolutes(:,1) = bounds(3) + Psolutes(:,1)*yrange

    ! random assigning the z-coordinate above or below the glideplane

    do i = 1, Nsolutes
       call random_number(rnd1)
       j = int(rnd1*ipos+.5)
       soltmp = solpos(j,:)
       Psolutes(i,:) = soltmp
       solpos(j,:)=solpos(ipos,:)
       ipos=ipos-1
    enddo
   
    !do i = 1, Nsolutes
    !   call random_number(updown)
    !   updown=sign(S_offset,(updown-0.5d0))
    !   call random_number(rnd1)
    !   rnd1=rnd1*xrange
    !   call random_number(rnd2)
    !   rnd2=rnd2*yrange
    !   Psolutes(i,:) = pervec*(bounds(1)+rnd1) +glidedir*(bounds(3)+rnd2) +sv*updown
    !enddo


!    n_gl = cross(sv,pervec)/norm(cross(sv,pervec))
!    
!    do i =1,Nsolutes
!       z_gl = vdot(Psolutes(i,:),pervec)
!       x_gl = vdot(Psolutes(i,:),n_gl)
!       y_gl = vdot(Psolutes(i,:),sv)
!       write(16,'(5E14.4)') Psolutes(i,3), Psolutes(i,1), Psolutes(i,2), z_gl, x_gl
!    enddo
!    stop
    
  end subroutine init_solutes


  ! ###################    	
  ! ###################    	
  subroutine solute_grid(Psolutes, r_grid, Ps_grid, nPS_grid, nx, ny, dx_sol)

    use param
    use utils
    implicit none
    
    integer      ::  i, j, r_grid(nngridtot,nngridtot,3), Ps_grid(nngridtot,nngridtot,Nsgridloc), &
         nPS_grid(nngridtot,nngridtot), nx, ny, isol
    real(kind=8) ::  Psolutes(Nstot, 3), xrange, yrange, &
         glidedir(3), bvnormal(3), dx_sol, rad_sol
      
    real (kind=8) :: n_gl(3), z_gl, x_gl, y_gl
    
    xrange = bounds(2) - bounds(1)
    yrange = bounds(4) - bounds(3)
    glidedir = -cross(pervec,sv)/norm(cross(pervec,sv))
    bvnormal = cross(bv,sv)/norm(cross(bv,sv))

    ! make a grid
    ! dx_sol is the spacing of the grid in both directions in the glide plane
    nx = int(xrange/dx_sol)+1
    ny = int(yrange/dx_sol)+1

    write(*,*) "nx, ny ", nx, ny
    
    do i = 1,nx-1
       do j =1,ny-1
          r_grid(i,j,:) = pervec*(bounds(1) +dx_sol*0.5d0 +(i-1)*dx_sol)
          r_grid(i,j,:) = r_grid(i,j,:) +glidedir*(bounds(3) +dx_sol*0.5d0 +(j-1)*dx_sol)
       enddo
    enddo
    i=nx
    do j =1,ny-1
       r_grid(i,j,:) = pervec*( bounds(1) +0.5d0*((i-1)*dx_sol + xrange) )
       r_grid(i,j,:) = r_grid(i,j,:) +glidedir*(bounds(3) +dx_sol*0.5d0 +(j-1)*dx_sol)
    enddo
    i=nx
    j=ny
    r_grid(i,j,:) = pervec*( bounds(1) +0.5d0*((i-1)*dx_sol + xrange) )
    r_grid(i,j,:) = r_grid(i,j,:) +glidedir*(bounds(3) +0.5d0*((j-1)*dx_sol +yrange))

    !   Psolutes(i,:) = pervec*(bounds(1)+rnd1) +glidedir*(bounds(3)+rnd2) +sv*updown

    ! assign the associated Psolutes points to each grid point
    ! all points within a raadius rad_sol
    rad_sol = 2.5*dx_sol
    do i = 1, nx
       do j = 1,ny
          nPS_grid(i,j) = 0
          do isol = 1,Nsolutes
             if(norm(Psolutes(isol,:)-r_grid(i,j,:)).lt.rad_sol) then
                nPs_grid(i,j) = nPs_grid(i,j) + 1
                Ps_grid(i,j,nPs_grid(i,j)) = isol
                if(nPs_grid(i,j).ge.Nsgridloc) then
                   write(*,*) "too many solutes"
                   exit
                endif
             endif
          enddo
       enddo
    enddo

!    n_gl = cross(sv,pervec)/norm(cross(sv,pervec))
!    
!    do i =1,Nsolutes
!       z_gl = vdot(Psolutes(i,:),pervec)
!       x_gl = vdot(Psolutes(i,:),n_gl)
!       y_gl = vdot(Psolutes(i,:),sv)
!       write(16,'(5E14.4)') Psolutes(i,3), Psolutes(i,1), Psolutes(i,2), z_gl, x_gl
!    enddo
!    stop

    
  end subroutine solute_grid


  ! ###################    	
  ! ###################
  

  subroutine update_gridpos(rn, r_grid, grid_pos, nx, ny, dx_sol)

    use param
    use utils
    implicit none
    
    integer      ::  i, j, r_grid(nngridtot,nngridtot,3), grid_pos(nntot,2), &
         nx, ny, isol, inode, i0,j0
    real(kind=8) :: rn(nntot,3), dx_sol
    
    do inode = 1, Nnodes
       ! check first old guess
       i0=grid_pos(inode,1)
       j0=grid_pos(inode,2)
       ! i,j
       i = i0
       j = j0
       if(norm(rn(inode,:)-r_grid(i,j,:)) .lt. sqrt(2.)*dx_sol) goto 111
       ! i,j+1
       if (j0+1.le.ny) then
          i = i0
          j = j0+1
          if(norm(rn(inode,:)-r_grid(i,j,:)) .lt. sqrt(2.)*dx_sol) goto 111
       endif
       !i+1,j+1
       if ((i0+1 .le. nx).and.(j0+1.le.ny)) then
          i = i0+1
          j = j0+1
          if(norm(rn(inode,:)-r_grid(i,j,:)) .lt. sqrt(2.)*dx_sol) goto 111
       endif
       !i-1,j+1
       if ((i0-1.ge.1).and.(j0+1.le.ny)) then
          i = i0-1
          j = j0+1
          if(norm(rn(inode,:)-r_grid(i,j,:)) .lt. sqrt(2.)*dx_sol) goto 111
       endif
       ! i+1,j
       if (i0+1.le.nx) then
          i=i0+1
          j = j0
          if(norm(rn(inode,:)-r_grid(i,j,:)) .lt. sqrt(2.)*dx_sol) goto 111
       endif
       ! i-1,j
       if (i0-1.ge.1) then
          i=i0-1
          j = j0
          if(norm(rn(inode,:)-r_grid(i,j,:)) .lt. sqrt(2.)*dx_sol) goto 111
       endif
       ! i-1,j-1
       if ((i0-1.ge.1).and.(j0-1.ge.1)) then
          i=i0-1
          j = j0-1
          if(norm(rn(inode,:)-r_grid(i,j,:)) .lt. sqrt(2.)*dx_sol) goto 111
       endif
       ! i,j-1
       if (j0-1.ge.1) then
          i = i0
          j = j0-1
          if(norm(rn(inode,:)-r_grid(i,j,:)) .lt. sqrt(2.)*dx_sol) goto 111
       endif
       ! i+1,j-1
       if ((i0+1.le.nx).and.(j0-1.ge.1)) then
          i = i0+1
          j = j0-1
          if(norm(rn(inode,:)-r_grid(i,j,:)) .lt. sqrt(2.)*dx_sol) goto 111
       endif
       do i = 1,nx
          do j =1,ny
             if(norm(rn(inode,:)-r_grid(i,j,:)) .lt. sqrt(2.)*dx_sol) goto 111
          enddo
       enddo
111    continue
       grid_pos(inode,2) = j
       grid_pos(inode,1) = i       
    enddo
    
  end subroutine update_gridpos

  
end module initialize
